public class CestoDeLembas{
    private int quantidade;
    private boolean dividir;
    
    public CestoDeLembas(int quantidade){
        this.quantidade = quantidade;
        podeDividir();
    }
    
    public boolean podeDividir(){
        this.dividir = this.quantidade % 2 == 0 ? true : false;
        return this.dividir;
    }
}
