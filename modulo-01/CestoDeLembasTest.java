import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CestoDeLembasTest{
    
    @Test
    public void cestoDeLembasDeveRetornarTrueQuandoNumeroPar(){
        CestoDeLembas cesto = new CestoDeLembas(4);
        assertTrue(cesto.podeDividir());
    }
    
    @Test
    public void cestoDeLembasDeveRetornarFalseQuandoNumeroImpar(){
        CestoDeLembas cesto = new CestoDeLembas(3);
        assertFalse(cesto.podeDividir());
    }
}
