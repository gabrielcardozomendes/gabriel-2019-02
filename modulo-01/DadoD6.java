import java.util.*;

public class DadoD6 implements Sortear{
    
    public int girarD6(){
        Random rand = new Random();
        int randomico = rand.nextInt(6) + 1;
        return randomico;
    }    
}
