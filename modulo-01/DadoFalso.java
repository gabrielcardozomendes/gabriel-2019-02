public class DadoFalso implements Sortear{
    private int valorFalso;
    
    public void simularValor(int valor){
        this.valorFalso = valor;
    }
    
    public int girarD6(){
        return this.valorFalso;
    }
}
