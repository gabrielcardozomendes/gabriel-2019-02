public class Dwarf extends Personagem{
    protected boolean escudoEquipado;
   
    public Dwarf(String nome){
        super(nome);
        vida = 110.0; 
        Item escudo = new Item(1, "Escudo");
        this.inventario.adicionarItem(escudo);
        escudoEquipado = false;
        super.dano = 10.0;
    }
     
    public boolean equiparEscudo(){
       if(this.inventario.buscarItem("Escudo")!=null){
            escudoEquipado=true;
            
            return escudoEquipado;
       }
       return escudoEquipado;
    }
        
    public void sofrerDano(){
        if(escudoEquipado){
            this.dano = this.dano / 2;
        }
        
        if(podeSofrerDano()){ 
            if(podeSofrerDano()){
            this.vida = this.vida >= this.dano ? this.vida - dano : 0.0;
            this.status = status.SOFREU_DANO;
            }
            
            if(this.vida == 0.0){
                status = Status.MORTO;
            }
        }
    }
    
    public String imprimirResultado(){
        return "Dwarf";
    }
}
