import java.util.*;

public class DwarfBarbaLonga extends Dwarf{
    
    public final ArrayList<DadoD6> NUMEROS_PERMITIDOS = new ArrayList(Arrays.asList(2, 6));
    private Sortear sorteador;
    
    public DwarfBarbaLonga(String nome){
        super(nome);
        sorteador = new DadoD6();
    }
    
    public DwarfBarbaLonga(String nome, Sortear sorteador){
        this(nome);
        this.sorteador = sorteador;
    }
    
    public boolean podeSofrerDano(){ 
        return NUMEROS_PERMITIDOS.contains(sorteador.girarD6());
    }
    
    public void sofrerDano(){
        boolean devePerderVida = sorteador.girarD6() <= 4;
        if(devePerderVida){
            super.sofrerDano();
        }
    }
}
