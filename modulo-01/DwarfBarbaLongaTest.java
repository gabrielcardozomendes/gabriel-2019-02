import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest{
    
    @Test
    public void dwarfBarbaLongaNaoDeveTomarDanoSeONumeroForValido(){
       DadoFalso dado = new DadoFalso();
       dado.simularValor(6);
       DwarfBarbaLonga barbudo = new DwarfBarbaLonga("Magni", dado);
       barbudo.sofrerDano();
       assertEquals(110.0, barbudo.getVida(),1e-9);
    }
}
