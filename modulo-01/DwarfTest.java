import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class DwarfTest{

    
    @Test 
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Baldur");
        assertEquals(110.0, dwarf.getVida(),1e-9);
    }

    @Test 
    public void dwarfPerde10DeVida(){
        Dwarf dwarf = new Dwarf("Baldur");
        dwarf.sofrerDano();  
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }
    
    @Test 
    public void dwarfPerdeTodaVida22Ataques(){
        Dwarf dwarf = new Dwarf("Baldur");
        for(int i = 0; i < 22; i++){
            dwarf.sofrerDano();
        }
        
        assertEquals(0.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNasceComStatus(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO,dwarf.getStatus());
    } 
    
    @Test
    public void dwarfPerdeVidaEContinuaVivo(){ 
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofrerDano();  
        assertEquals(Status.SOFREU_DANO,dwarf.getStatus());
    }
    
    
    @Test
    public void dwarfPerdeVidaEMorre(){
        Dwarf dwarf = new Dwarf("Gimli");
        for (int i=0; i<24; i++){
            dwarf.sofrerDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
        assertEquals(0.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf novoAnao = new Dwarf("Baldur");
        assertEquals("Escudo", novoAnao.inventario.buscarItem("Escudo").getDescricao());
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf novoAnao = new Dwarf("Baldur");
        novoAnao.equiparEscudo();
        novoAnao.sofrerDano();
        assertEquals(5.0, novoAnao.dano, 1e-9);
        assertEquals(105.0, novoAnao.vida, 1e-9);
    }
    
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf novoAnao = new Dwarf("Baldur");
        novoAnao.sofrerDano();
        assertEquals(10.0, novoAnao.dano, 1e-9);
        assertEquals(100.0, novoAnao.vida, 1e-9);
    }
}
