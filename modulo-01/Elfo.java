
public class Elfo extends Personagem implements Comparable<Elfo>{
    protected int qtdExperienciaPorAtaque;
    private static int umNovoIntegranteDoExercito;
    
    {
        this.qtdExperienciaPorAtaque = 1;
    }
    
    protected Elfo(String nome){
        super(nome);
        this.status = Status.RECEM_CRIADO;
        this.vida = 100.0;
        super.inventario.adicionarItem(new Item(2, "Flecha"));
        super.inventario.adicionarItem(new Item(1, "Arco"));
        Elfo.umNovoIntegranteDoExercito++;
        this.qtdExperienciaPorAtaque = 1;
    }
    
    protected void finalize() throws Throwable{
        Elfo.umNovoIntegranteDoExercito--;
    }
    
    public static int getQtdElfos(){
        return Elfo.umNovoIntegranteDoExercito;
    }
    
    protected Item getFlecha(){
        return this.inventario.buscarItem("Flecha"); 
    }

    protected int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    protected boolean podeAtirarFlecha(){
        return this.getQtdFlecha() > 0;
    }
    
    protected void atirarFlecha(Dwarf dwarf){
        int qtdAtual = getQtdFlecha();
        
        if(podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            dwarf.sofrerDano();
            this.aumentaXP();
        }
    }
    
    public void atacar(Dwarf dwarf){
        this.atirarFlecha(dwarf);
    }
    
    protected String imprimirResultado(){
        return "Elfo";
    }
    
    public int compareTo(Elfo elfo){
        if(this.getQtdFlecha() > elfo.getQtdFlecha()){
            return -1;
        }
        
        if(this.getQtdFlecha() < elfo.getQtdFlecha()){
            return 1;
        }
        return 0;
    }   
}
