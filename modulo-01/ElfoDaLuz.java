import java.util.*;

public class ElfoDaLuz extends Elfo{
    private int qtdDeAtaques = 0;
    private final double QTD_VIDA_GANHA = 10;
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(Arrays.asList("Espada de Galvorn"));
    
    public ElfoDaLuz(String nome){
        super(nome);
        this.dano = 21.0;
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_VALIDAS.get(0)));
    }
    
    public Item getEspada(){
        return this.getInventario().buscarItem(DESCRICOES_VALIDAS.get(0));
    }
    
    private boolean devePerderVida(){
        return this.qtdDeAtaques % 2 != 0;
    }
    
    private boolean deveGanharVida(){
        return qtdDeAtaques % 2 == 0;
    }
    
    public void atacarComEspada(Dwarf dwarf){
        dwarf.sofrerDano();
        qtdDeAtaques++;
        this.aumentaXP();
        
        if(this.vida == 0){
            this.status = Status.MORTO;
        }else if(devePerderVida()){
            this.vida -= 21.0;
        }else if(deveGanharVida()){
            this.vida += QTD_VIDA_GANHA;
        }
    }
    
    public void perderItem(Item item){
       boolean possoPerder = !DESCRICOES_VALIDAS.contains(item.getDescricao()); 
        
       if(possoPerder){
           super.perderItem(item);
       }
   }
}
