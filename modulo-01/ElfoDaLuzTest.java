import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    
    @Test 
    public void elfoDaLuzDevePerderVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf baldur = new Dwarf("Baldur");
        feanor.atacarComEspada(baldur);
        assertEquals(79.0, feanor.getVida(), 1e-9);
        assertEquals(100.0, baldur.getVida(), 1e-9);
    }
    
    @Test
    public void elfoDaLuzDeveGanharVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf baldur = new Dwarf("Baldur");
        feanor.atacarComEspada(baldur);
        feanor.atacarComEspada(baldur);
        assertEquals(89.0, feanor.getVida(), 1e-9);
        assertEquals(90.0, baldur.getVida(), 1e-9);
    }
}
