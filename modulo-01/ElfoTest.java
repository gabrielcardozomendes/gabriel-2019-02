import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test 
    public void atirarFlechasDiminuirFlechaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Baldur");
        novoElfo.atirarFlecha(novoAnao);
        
    } 
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Baldur");
        novoElfo.atirarFlecha(novoAnao);
        novoElfo.atirarFlecha(novoAnao);
        novoElfo.atirarFlecha(novoAnao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    }
     
    @Test
    public void elfosNascemCom2Flechas(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(2, novoElfo.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void criarUmElfoIncrementaContadorUmaVez(){
        new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }
    
    @Test
    public void criarDoisElfosIncrementaContadorDuasVez(){
        new Elfo("Legolas");
        new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }
}
