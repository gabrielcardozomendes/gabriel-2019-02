public class ElfosNoturnos extends Elfo{
    
    public ElfosNoturnos(String nome){
        super(nome); 
        this.qtdExperienciaPorAtaque = 3;
        this.dano = 15.0;
    }
    
    @Override
    public void aumentaXP(){
        this.experiencia += 3;
    }
    
    
    @Override
    public void atirarFlecha(Dwarf dwarf){
        int qtdAtual = getQtdFlecha(); 
        
        if(this.podeSofrerDano() && super.podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            dwarf.sofrerDano();
            this.aumentaXP();
            super.sofrerDano();
        }
    }
    
    @Override
    public void atacar(Dwarf dwarf){
        this.atirarFlecha(dwarf);
    }
}
