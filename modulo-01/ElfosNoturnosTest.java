import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfosNoturnosTest{
    @Test
    public void elfoAtiraFlechaEGanhaTriploDeXP()
    {
        ElfosNoturnos eredin = new ElfosNoturnos("Eredin");
        Dwarf baldur = new Dwarf("Baldur");
        eredin.atirarFlecha(baldur);
        assertEquals(3, eredin.getExperiencia());
    }
    
    @Test
    public void elfoAtiraFlechaEGanhaTriploDeXPDuasVezes()
    {
        ElfosNoturnos eredin = new ElfosNoturnos("Eredin");
        Dwarf baldur = new Dwarf("Baldur");
        eredin.atirarFlecha(baldur);
        eredin.atirarFlecha(baldur);
        assertEquals(6, eredin.getExperiencia());
    }
    
    @Test
    public void elfoAtiraFlechaEPerde15UnidadesNaVida()
    {
        ElfosNoturnos eredin = new ElfosNoturnos("Eredin");
        Dwarf baldur = new Dwarf("Baldur");
        eredin.atirarFlecha(baldur);
        assertEquals(85.0, eredin.getVida(), 1e-9);
    }
    
    @Test
    public void elfoAtiraFlechaEPerde15UnidadesNaVidaDuasVezes()
    {
        ElfosNoturnos eredin = new ElfosNoturnos("Eredin");
        Dwarf baldur = new Dwarf("Baldur");
        eredin.atirarFlecha(baldur);
        eredin.atirarFlecha(baldur);
        assertEquals(70.0, eredin.getVida(), 1e-9);
    }
    
    @Test
    public void elfoNoturnoAtira7FlechasEMorre()
    {
        ElfosNoturnos eredin = new ElfosNoturnos("Eredin");
        Dwarf baldur = new Dwarf("Baldur");
        eredin.getFlecha().setQuantidade(100);
        
        for(int i = 0; i < 7; i++){
            eredin.atirarFlecha(baldur);
        }
        
        assertEquals(0, eredin.getVida(), 1e-9);
        assertEquals(Status.MORTO, eredin.getStatus());
        assertEquals(93, eredin.getQtdFlecha());
    }
}
