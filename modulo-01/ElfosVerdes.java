import java.util.ArrayList;
import  java.util.Arrays;
public class ElfosVerdes extends Elfo{
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(Arrays.asList("Espada de aço valiriano", "Arco de Vidro", "Flecha de Vidro"));
       
    public ElfosVerdes(String nome){
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }
   
   @Override
   public void ganharItem(Item item){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(descricaoValida){
            this.inventario.adicionarItem(item);
        }
   }
   
   @Override
   public void perderItem(Item item){
       boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
       if(descricaoValida){
           this.inventario.removerItem(item);
       }
   }
}
