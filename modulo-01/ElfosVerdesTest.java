import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfosVerdesTest{
    
    @Test
    public void atirarFlechaRecebe2Xp(){
        Dwarf baldur = new Dwarf("Baldur");
        ElfosVerdes verde = new ElfosVerdes("Malfurion");
        verde.atirarFlecha(baldur);
        assertEquals(2, verde.getExperiencia());
    }
    
    @Test
    public void soPodeGanharArcoDeVidroFlechaDeVidroEspadaDeAcoValiriano(){
        ElfosVerdes verde = new ElfosVerdes("Malfurion");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        verde.ganharItem(arcoDeVidro);
        verde.inventario.getItens();
        assertEquals(arcoDeVidro, verde.inventario.obterPosicao(2));
    }
    
    @Test
    public void elfoPerdeItemDescricaoValida()
    {
        ElfosVerdes elfo = new ElfosVerdes("Samblar");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        elfo.ganharItem(arcoDeVidro);
        elfo.perderItem(arcoDeVidro);
        assertNull(elfo.getInventario().buscarItem(arcoDeVidro));
    }
    
    @Test
    public void elfoVerdeGanhaItemInvalido(){
        ElfosVerdes verde = new ElfosVerdes("Malfurion");
        Item arcoDeMadeira = new Item(1, "Arco de Madeira");
        verde.ganharItem(arcoDeMadeira);
        Inventario inventario = verde.getInventario();
        assertNull(verde.inventario.buscarItem("Arco de Madeira"));
    }
    
    @Test
    public void elfoPerdeItemDescricaoInvalida()
    {
        ElfosVerdes elfo = new ElfosVerdes("Malfurion");
        Item arco = new Item(1, "Arco");
        elfo.perderItem(arco);
        assertEquals(arco, elfo.getInventario().obterPosicao(1));
    }
}
