import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class EstrategiasDeAtaqueTest {

    @Test
    public void getNoturnosPorUltimo() {
        EstrategiasDeGuerra estrategiasDeAtaque = new EstrategiasDeGuerra();
        ArrayList<Elfo> atacantes = new ArrayList<>();
        Elfo elfo1 = new ElfosVerdes("Legolas");
        Elfo elfo2 = new ElfosNoturnos("Legolas");
        Elfo elfo3 = new ElfosVerdes("Legolas");
        Elfo elfo4 = new ElfosNoturnos("Legolas");
        Elfo elfo5 = new ElfosNoturnos("Legolas");
        Elfo elfo6 = new ElfosVerdes("Legolas");
        Elfo elfo7 = new ElfosVerdes("Legolas");
        Elfo elfo8 = new ElfosNoturnos("Legolas");
        Elfo elfo9 = new ElfosVerdes("Legolas");
        Elfo elfo10 = new ElfosNoturnos("Legolas");
        
        atacantes.add(elfo1);
        atacantes.add(elfo2);
        atacantes.add(elfo3);
        atacantes.add(elfo4);
        atacantes.add(elfo5);
        atacantes.add(elfo6);
        atacantes.add(elfo7);
        atacantes.add(elfo8);
        atacantes.add(elfo9);
        atacantes.add(elfo10);
        
        estrategiasDeAtaque.alterarEstrategia(TipoEstrategia.NOTURNOS_POR_ULTIMO);
        
        ArrayList<Elfo> atacantesOrdenados = estrategiasDeAtaque.getOrdemDeAtaque(atacantes);
        
        assertEquals(elfo9, atacantesOrdenados.get(0));
        assertEquals(elfo7, atacantesOrdenados.get(1));
        assertEquals(elfo6, atacantesOrdenados.get(2));
        assertEquals(elfo3, atacantesOrdenados.get(3));
        assertEquals(elfo1, atacantesOrdenados.get(4));
        assertEquals(elfo2, atacantesOrdenados.get(5));
        assertEquals(elfo4, atacantesOrdenados.get(6));
        assertEquals(elfo5, atacantesOrdenados.get(7));
        assertEquals(elfo8, atacantesOrdenados.get(8));
        assertEquals(elfo10, atacantesOrdenados.get(9));
    }

    @Test
    public void getAtaqueIntercalado() {
        EstrategiasDeGuerra estrategiasDeAtaque = new EstrategiasDeGuerra();
        
        ArrayList<Elfo> atacantes = new ArrayList<>();
        
        Elfo elfo1 = new ElfosVerdes("Legolas");
        Elfo elfo2 = new ElfosVerdes("Legolas");
        Elfo elfo3 = new ElfosVerdes("Legolas");
        Elfo elfo4 = new ElfosNoturnos("Legolas");
        Elfo elfo5 = new ElfosNoturnos("Legolas");
        
        atacantes.add(elfo1);
        atacantes.add(elfo2);
        atacantes.add(elfo3);
        atacantes.add(elfo4);
        atacantes.add(elfo5);
        
        estrategiasDeAtaque.alterarEstrategia(TipoEstrategia.ATAQUE_INTERCALADO);
        ArrayList<Elfo> atacantesOrdenados = estrategiasDeAtaque.getOrdemDeAtaque(atacantes);

        assertEquals(elfo1, atacantesOrdenados.get(0));
        assertEquals(elfo4, atacantesOrdenados.get(1));
        assertEquals(elfo2, atacantesOrdenados.get(2));
        assertEquals(elfo5, atacantesOrdenados.get(3));
    }

    @Test
    public void personalizado() {
        EstrategiasDeGuerra estrategiasDeAtaque = new EstrategiasDeGuerra();
        ArrayList<Elfo> atacantes = new ArrayList<>();
        
        Elfo noturno1 = new ElfosNoturnos("Noturno 1");
        Elfo noturno2 = new ElfosNoturnos("Noturno 2");
        
        Elfo verde1 = new ElfosVerdes("Verde 1");
        Elfo verde2 = new ElfosVerdes("Verde 2");
        Elfo verde3 = new ElfosVerdes("Verde 3");
        
        noturno1.getFlecha().setQuantidade(3);
        noturno2.getFlecha().setQuantidade(12);
        verde1.getFlecha().setQuantidade(0);
        verde2.getFlecha().setQuantidade(40);
        verde3.getFlecha().setQuantidade(50);
        
        atacantes.add(noturno1);
        atacantes.add(noturno2);
        atacantes.add(verde1);
        atacantes.add(verde2);
        atacantes.add(verde3);

        estrategiasDeAtaque.alterarEstrategia(TipoEstrategia.PERSONALIZADO);
        ArrayList<Elfo> atacantesOrdenados = estrategiasDeAtaque.getOrdemDeAtaque(atacantes);

        assertEquals(verde3, atacantesOrdenados.get(0));
        assertEquals(verde2, atacantesOrdenados.get(1));
        assertEquals(noturno1, atacantesOrdenados.get(2));
    }

    @Test
    public void getOrdemDeAtaque() {

    }
}