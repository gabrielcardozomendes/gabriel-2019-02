import java.util.*;
import java.util.stream.Collectors;
import java.util.Collections;

public class EstrategiasDeGuerra implements Estrategias{
    private int contador = 0;
    private TipoEstrategia estrategiaAtual;
    
    private boolean elfoVivo(Elfo elfo){
        return elfo.getVida() > 0;
    }
    
    public void alterarEstrategia(TipoEstrategia novaEstrategia){
        this.estrategiaAtual = novaEstrategia;
     }
    
    public ArrayList<Elfo> noturnosPorUltimo(ArrayList<Elfo> atacantes){
         LinkedList listaNoturnosPorUltimo = new LinkedList(); 
         
         for(Elfo elfo : atacantes){
            if(elfoVivo(elfo)){
                if(elfo instanceof ElfosNoturnos){
                    listaNoturnosPorUltimo.addLast(elfo);
                }else{
                    listaNoturnosPorUltimo.addFirst(elfo);
                }
            }
         }
         return new ArrayList<>(listaNoturnosPorUltimo);
    }
    
    public ArrayList<Elfo> ataqueIntercalado(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosNoturnos = atacantes.stream()
                .filter((Elfo)-> Elfo instanceof ElfosNoturnos).collect(Collectors.toCollection(ArrayList::new));
                
        ArrayList<Elfo> elfosVerdes = atacantes.stream()
                .filter((Elfo)-> Elfo instanceof ElfosVerdes).collect(Collectors.toCollection(ArrayList::new));
                
        ArrayList<Elfo> listaIntercalados = new ArrayList<>();
        
        while(!elfosNoturnos.isEmpty() && !elfosVerdes.isEmpty()){
            listaIntercalados.add(elfosVerdes.get(0));
            listaIntercalados.add(elfosNoturnos.get(0));
            elfosNoturnos.remove(0);
            elfosVerdes.remove(0);
        }
        
        return listaIntercalados;
    }

    public ArrayList<Elfo> personalizado(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> listaPersonalizados = new ArrayList<>();
        
        ArrayList<Elfo> elfosAptos = atacantes.stream()
                .filter((elfo)->elfo.podeAtirarFlecha() && elfo.getVida()>0).collect(Collectors.toCollection(ArrayList::new));
                
        int limiteElfosNoturnos = (int)(elfosAptos.size() * 0.3);
        int qtdElfosNoturnos=0;

        for(Elfo elfo : elfosAptos){
            if(elfo instanceof ElfosNoturnos){
                if(qtdElfosNoturnos < limiteElfosNoturnos) {
                    listaPersonalizados.add(elfo);
                    qtdElfosNoturnos++;
                }
            }
            else{
                listaPersonalizados.add(elfo);
            }

        }
        Collections.sort(listaPersonalizados);
        return listaPersonalizados;
    }

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        
        switch (estrategiaAtual) {
            case NOTURNOS_POR_ULTIMO:
                atacantes = noturnosPorUltimo(atacantes);
                break;
            case ATAQUE_INTERCALADO:
                atacantes = ataqueIntercalado(atacantes);
                break;
            case PERSONALIZADO:
                atacantes = personalizado(atacantes);
                break;
        }
        
        return atacantes;
    }
}
