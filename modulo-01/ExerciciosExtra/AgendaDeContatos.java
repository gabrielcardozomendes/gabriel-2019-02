package ExerciciosExtra;

import java.util.*;

public class AgendaDeContatos{
    private HashMap<String, String> contatos;
    private String qualquer;
    public AgendaDeContatos(){
        this.contatos = new LinkedHashMap<>();
    }
    
    public void adicionarContato(String nome, String telefone){
        this.contatos.put(nome, telefone);
    }
    
    public String consultarNome(String nome){
        return this.contatos.get(nome);
    }
    
    public String consultarTelefone(String telefone){
        for(HashMap.Entry<String, String> par : contatos.entrySet()){
            if(par.getValue().equals(telefone)){
                return par.getKey();
            }
        }
        return null;
    }
    
    public String csv(){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for(HashMap.Entry<String, String> par : contatos.entrySet()){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
        }
        return builder.toString();
    }
}
