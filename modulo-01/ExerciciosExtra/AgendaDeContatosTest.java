package ExerciciosExtra;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaDeContatosTest{
    
    @Test 
    public void adicionarContatoEPesquisar(){
        AgendaDeContatos agenda = new AgendaDeContatos();
        agenda.adicionarContato("Agustinho", "88888888");
        agenda.adicionarContato("Carrara", "99999999");
        assertEquals("88888888", agenda.consultarNome("Agustinho"));
    }
    
    @Test 
    public void adicionarContatoEPesquisarPorTelefone(){
        AgendaDeContatos agenda = new AgendaDeContatos();
        agenda.adicionarContato("Agustinho", "88888888");
        agenda.adicionarContato("Carrara", "99999999");
        assertEquals("Carrara", agenda.consultarTelefone("99999999"));
    }
    
    @Test 
    public void adicionarContatoEGerarCSV(){
        AgendaDeContatos agenda = new AgendaDeContatos();
        agenda.adicionarContato("Agustinho", "88888888");
        agenda.adicionarContato("Carrara", "99999999");
        String separador = System.lineSeparator();
        String esperado = String.format("Agustinho,88888888%sCarrara,99999999%s", separador, separador);
        assertEquals(esperado, agenda.csv());
    }
}