import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test 
    public void alistarUmElfoVerde(){
        Exercito exercito = new Exercito();
        ElfosVerdes malfurion = new ElfosVerdes("Malfurion");
        exercito.alistar(malfurion);
        assertEquals(malfurion, exercito.getElfos().get(0));
    }
    
    @Test
    public void alistarUmElfoNoturno(){
       Exercito exercito = new Exercito();
       ElfosNoturnos eredin = new ElfosNoturnos("Eredin");
       exercito.alistar(eredin);
       assertEquals(eredin, exercito.getElfos().get(0));
    }
    
    @Test
    public void alistarElfoInvalido(){
       Exercito exercito = new Exercito();
       ElfoDaLuz elfo = new ElfoDaLuz("elfo");
       exercito.alistar(elfo);
       assertTrue(exercito.getElfos().isEmpty());
    } 
    
    @Test 
    public void buscarElfoPorStatus(){
        Exercito exercito = new Exercito();
        Elfo legolas = new ElfosNoturnos("Legolas");
        exercito.alistar(legolas);
        assertEquals(legolas, exercito.buscar(legolas.getStatus()).get(0));
    }
    
    @Test
    public void quantidadeDeElfosNoExercito(){
        
        Exercito exercito = new Exercito();
        
        Elfo legolas1 = new Elfo("Legolas");
        Elfo legolas2 = new Elfo("Legolas");
        Elfo legolas3 = new Elfo("Legolas");
        Elfo legolas4 = new Elfo("Legolas");
        Elfo legolas5 = new Elfo("Legolas");
        Elfo legolas6 = new Elfo("Legolas");
        Elfo legolas7 = new Elfo("Legolas");
        
        exercito.alistar(legolas1);
        exercito.alistar(legolas2);
        exercito.alistar(legolas3);
        exercito.alistar(legolas4);
        exercito.alistar(legolas5);
        exercito.alistar(legolas6);
        exercito.alistar(legolas7);
        
        assertEquals(7, legolas7.getQtdElfos(), 1e-9);
    }
}   
