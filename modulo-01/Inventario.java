import java.util.*;

public class Inventario{
    private ArrayList<Item> lista = new ArrayList<>();
    private TipoOrdenacao tipo;
    
    public Inventario(int quantidade){
        this.lista = new ArrayList<>(quantidade);
    }
    
    public ArrayList<Item> getItens(){
        return this.lista;
    }
    
    public void adicionarItem(Item item){
        this.lista.add(item);         
    }
    
    public String retornaNome(){
        StringBuilder descricoes = new StringBuilder();
        for(int i = 0; i < this.lista.size(); i++){
            Item item = this.lista.get(i);
            if(item != null){
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        
        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() -1)) : descricoes.toString());
    }
    
    public Item obterPosicao(int posicao){
        return this.lista.get(posicao);
    }
    
    public void removerItem(Item item){
        this.lista.remove(item);
    }
    
    public Item getItemMaiorQuantidade(){
        int maior = 0;
        int posicao = 0;
        for(int i = 0; i < this.lista.size(); i++){
            Item item = this.lista.get(i);
            if(item != null){
                if(item.getQuantidade() > maior){
                maior = item.getQuantidade();
                posicao = i;
                }
            }
        }
        return this.lista.size() > 0 ? this.lista.get(posicao) : null;
    }
    
    public Item buscarItem(String descricao){
        for(Item itemAtual : this.lista){
           boolean encontrei = itemAtual.getDescricao().equals(descricao);
           if(encontrei){
              return itemAtual;
           }
        }
       return null;
    }
    
        public Item buscarItem(Item item){
        for(Item itemAtual : this.lista){
           boolean encontrei = itemAtual.equals(item);
           if(encontrei){
              return itemAtual;
           }
        }
       return null;
    }
    
    public void inverterInventario(){
        for(int i = lista.size(); i < lista.size(); i--){
            lista.set(i, lista.get(i));
        }
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao tipoOrdenacao){
        for(int i=0; i<this.lista.size(); i++){
           for (int j=0; j<this.lista.size()-1; j++){
               Item atual = this.lista.get(j);
               Item proximo = this.lista.get(j+1);
               boolean deveTrocar = tipoOrdenacao == TipoOrdenacao.ASC ? 
               atual.getQuantidade() > proximo.getQuantidade():
               atual.getQuantidade() < proximo.getQuantidade();        
               if(deveTrocar){
                   Item itemTrocado = atual;
                   this.lista.set(j,proximo);
                   this.lista.set(j+1, itemTrocado); 
                }
           }
       }
    }
    
    public void unir(ArrayList segundoInventario){
        ArrayList inventarioUnido = new ArrayList<>(lista.size() + segundoInventario.size());
        inventarioUnido.add(lista);
        inventarioUnido.add(segundoInventario);
    }
    
    public ArrayList diferenciar(ArrayList inventario, ArrayList segundoInventario){
        ArrayList diferenca = new ArrayList<>();
        
        int tamanhosIrados = inventario.size() + segundoInventario.size();
        
        for(int i = 0; i < tamanhosIrados; i++){
            if(inventario.get(i) != segundoInventario.get(i)){
                diferenca.add(inventario.get(i));
            }
            
            if(segundoInventario.get(i) != inventario.get(i)){
                diferenca.add(segundoInventario.get(i));
            }
        }
        
        return diferenca;
    }
    
    public ArrayList cruzar (ArrayList inventario, ArrayList segundoInventario){
        ArrayList cruzar  = new ArrayList<>();
        
        int tamanhosIrados = inventario.size() + segundoInventario.size();
        
        for(int i = 0; i < tamanhosIrados; i++){
            if(inventario.get(i) == segundoInventario.get(i)){
                cruzar .add(inventario.get(i));
            }
            
            if(segundoInventario.get(i) == inventario.get(i)){
                cruzar .add(segundoInventario.get(i));
            }
        }
        
        return cruzar ;
    }
}
