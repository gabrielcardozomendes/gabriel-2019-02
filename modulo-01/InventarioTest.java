import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class InventarioTest{
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem(espada);
        assertEquals(espada, inventario.obterPosicao(0));
    }
    
    @Test
    public void adicionarDoisItem(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(espada, inventario.obterPosicao(0));
        assertEquals(escudo, inventario.obterPosicao(1));
    }
    
    @Test
    public void obterItemNaPrimeiraPosicao(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem(espada);
        assertEquals(espada, inventario.obterPosicao(0));
    }
    
    @Test
    public void obterItemNaoAdicionado(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        assertEquals(null,inventario.buscarItem("Espada"));
    }
    
    
    @Test
    public void removerItem(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.removerItem(espada);
        assertNull(inventario.buscarItem(espada));
    }
    
    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.removerItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(escudo, inventario.obterPosicao(0));
    }
    
    @Test 
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(armadura);
        assertEquals("Espada,Escudo,Armadura", inventario.retornaNome());
    }
    
    @Test 
    public void getDescricoesNenhumItem(){
        Inventario inventario = new Inventario(20);
        assertEquals("", inventario.retornaNome());
    }
    
    
    
    
    //getItemMaiorQuantidadeComVarios
    @Test
    public void getItemMaiorQuantidadeComVarios(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(3, "Escudo");
        Item armadura = new Item(10, "Armadura");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(armadura);
        assertEquals(armadura, inventario.getItemMaiorQuantidade());
    }
    
    //getItemMaiorQuantidadeInventarioVazio
    @Test
    public void getItemMaiorQuantidadeInventarioVazio(){
        Inventario inventario = new Inventario(20);
        assertNull(inventario.getItemMaiorQuantidade());
    }
    
    //getItemMaiorQuantidadeComItensComMesmaQuantidade  
    @Test
    public void getItemMaiorQuantidadeComItensComMesmaQuantidade(){
        Inventario inventario = new Inventario(20);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(armadura);
        assertEquals(espada, inventario.getItemMaiorQuantidade());
    }
}
