import java.util.*;

public class PaginadorInventario{
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
        
    /*public int getPular(){
        return marcador;
    }*/
    
    public void setPular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar(int limite){
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + limite;
        for(int i = this.marcador; i<fim && i<this.inventario.getItens().size();i++){
            subConjunto.add(this.inventario.obterPosicao(i));
        }
        return subConjunto;
    }
    
}
