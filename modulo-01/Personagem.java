import java.util.ArrayList;

public abstract class Personagem{
   protected String nome;
   protected double vida;
   protected int experiencia;
   protected Inventario inventario; 
   protected Status status;
   protected double dano;
   
   {
       this.status = Status.RECEM_CRIADO;
       this.inventario = new Inventario(0);
       dano = 0.0;
   }
   
   public Personagem(String nome){
       this.nome = nome;
   }
   
   public String getNome(){
       return this.nome;
   }
   
   public void setNome(String nome){
       this.nome = nome;
   }
   
   public double getVida(){
       return this.vida;
   }
   
   public void aumentaXP(){
        this.experiencia++;
   }
   
   public int getExperiencia(){
       return this.experiencia;
   }
   
   public Status getStatus(){
       return this.status;
   }
   
   public boolean podeSofrerDano(){
        return this.vida > 0;
   }
   
   public void ganharItem(Item item){
        this.inventario.adicionarItem(item);
   }
  
   public void perderItem(Item item){
       this.inventario.removerItem(item);
   }
   
   public Inventario getInventario(){
       return this.inventario;
   }
   
   public double calcularDano(){
       return this.dano;
   }
   
   public void sofrerDano(){
        this.vida -= this.vida >= this.dano ? calcularDano() : this.vida;
        
        if(this.vida == 0.0){
            this.status = Status.MORTO;
        }
        else {
            this.status = Status.SOFREU_DANO;
        }
   }
   
   protected abstract String imprimirResultado();
}
