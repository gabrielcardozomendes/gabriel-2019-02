function testarForm(form){
    if(form.nome.value == "" || form.nome.value.length < 10){
        alert("O campo nome é obrigatório.")
        form.nome.focus();
        form.nome.style.borderColor = "red";
        return false;
    }else{
        form.nome.style.borderColor = "Green";
    }

    if((form.email.value == "") || (form.email.value.indexOf('@') == -1) || (form.email.value.indexOf('.') == -1)){
        alert("O campo e-mail é obrigatório.")
        form.email.focus();
        return false;
    }else{
        form.email.style.borderColor = "Green";
    }

    if(form.telefone.value == "" ||form.telefone.value.length < 8){
        alert("O campo telefone é obrigatório.")
        form.telefone.focus();
        form.telefone.style.borderColor = "red";
        return false;
    }else{
        form.nome.style.borderColor = "Green";
    }

    if(form.mensagem.value == ""){
        alert("O campo mensagem é obrigatório.")
        form.mensagem.focus();
        return false;
    }else{
        form.nome.style.borderColor = "Green";
    }
}