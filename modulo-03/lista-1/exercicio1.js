//Exercício - 1

 const circulo = {
        raio: 0,
        tipoCalculo: ""
 }

 const { raio, tipoCalculo } = circulo;

 function calcularCirculo(raio, tipoCalculo, d = 0) {

    if(tipoCalculo == "A" || tipoCalculo == "a"){
        return Math.PI * Math.pow(raio);

    }else if(tipoCalculo == "C" || tipoCalculo == "c"){
        return d * Math.PI;
    }
 }

 console.log(`A = ${calcularCirculo(3, "A")}`);
 console.log(`A = ${calcularCirculo(3, "C", 5)}`);

