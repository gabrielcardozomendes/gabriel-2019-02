//Exercício - 2

let num;

/*function naoBissexto(num) {
    if(num % 400 === 0 || (num % 4 == 0 && num % 100 != 0)){
        return false;
    }else{
        return true;
    }
}*/

//Arrow Function
//let naoBissexto = num => num % 400 === 0 || (num % 4 == 0 && num % 100 != 0) ? false : true;

const testes = {
    diaAula: "Segundo",
    local: "DBC",
    naoBissexto(num) {
        return num % 400 === 0 || (num % 4 == 0 && num % 100 != 0) ? false : true;
    }
}

console.log(testes.naoBissexto(2019));

console.log(testes.naoBissexto(2016));