//Exercício - 4

/*function adicionar(op1){
    return function(op2){
        return op1 + op2;
    }
}*/

//Arrow Function 
let adicionar = op1 => op2 => op1 + op2;

console.log(adicionar(3)(4));//7
console.log(adicionar(5642)(8749));//14391

//exemplo funcionalidade do método que tem o nome parecido com curry

/* const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
const is_divisivel3 = divisivelPor(3);
console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12));
console.log(is_divisivel3(20));
console.log(is_divisivel3(11));
console.log(is_divisivel3(12));