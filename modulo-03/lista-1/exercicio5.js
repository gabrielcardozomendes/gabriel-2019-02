//Exercício - 5

function arredondar(numero, precisao = 2) {
    const fator = Math.pow( 10, precisao );
    return Math.ceil(numero * fator) / fator;
}

function imprimirBRL(numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = ".";
    let separadorDecimal = ",";

    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;

    let c =  1;
    while(parteInteiraString.length > 0){
        if(c % qtdCasasMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`);
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        }else if(parteInteiraString.length < qtdCasasMilhares){
            stringBuffer.push( parteInteiraString );
            parteInteiraString = '';
        }
        c++;
    }
    stringBuffer.push( parteInteiraString );
    
    let decmalString = parteDecimal.toString().replace('0.','').padStart(2, '0');
    return `${ parteInteira >= 0 ? 'R$ ' : '-R$ ' }${ stringBuffer.reverse().join('') }${ separadorDecimal }${ decmalString }`;
}

console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.9));
console.log(imprimirBRL(-3498.9));
console.log(imprimirBRL(2313477.0135));