
function multiplicar(multiplicador, ...valores){
    resultado = valores.map(valor => multiplicador * valor)  
    return resultado;
}

console.log(multiplicar(5, 3, 4, 5));