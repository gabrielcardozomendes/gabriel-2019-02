import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import './App.css';

import NetBlits from '../src/components/TelaNetBlits';
import ReactMirror from '../src/components/TelaMirror';
import Login from '../src/components/Login';

import { PrivateRoute } from './components/PrivateRoute';

export default class App extends Component {
  render(){
    return (
      <Router>
        <React.Fragment>
          <PrivateRoute path="/" exact component={ PaginaInicial } />
          <Route path="/login" component={ Login } />
          <Route path="/mirror" exact component={ ReactMirror } />
          <Route path="/netBlits" component={ NetBlits } />
        </React.Fragment>
      </Router>
    );
  }
}


const PaginaInicial = () => 
  <section className="inicio">
    <Link className="mirror" to="/mirror" >React Mirror</Link>
    <Link className="netBlits" to="/netBlits">NetBlits</Link>
  </section>
