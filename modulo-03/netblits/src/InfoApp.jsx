import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './App.css';
import ListaSeries from './models/ListaSeries'
import TelaInicial from './components/TelaInicial'
import TelaNetBlits from './components/TelaNetBlits'

class InfoApp extends Component {
  constructor( props ) {
    super( props )
    this.listaSeries = new ListaSeries()
    this.state = {
      serie: this.listaSeries.series
    }
  }

  testador() {
    let seriesInvalidas = Array.prototype.seriesInvalidas()
    console.log(seriesInvalidas)

    let seriesFiltradasAno = Array.prototype.filtrarPorAno(2016)
    console.log(seriesFiltradasAno)

    let seriesFiltradasNome = Array.prototype.filtrarPorNome('Paul')
    console.log(seriesFiltradasNome)

    let mediaDeEpisodios = Array.prototype.mediaDeEpisodios()
    console.log(mediaDeEpisodios)

    let salario = Array.prototype.totalSalarios(2)
    console.log(salario)

    let genero = Array.prototype.queroGenero('Caos')
    console.log(genero)

    let creditos = Array.prototype.creditos(0)
    console.log(creditos)

    let hastag = this.listaSeries.hashTagSecreta()
    console.log(hastag)
  }

  render() {
    const { series } = this.state
    return (
      <div className="App">
        <div className="App-Header">
          <TelaInicial/>
        </div>
      </div>
    )
  }
}

export default InfoApp;
