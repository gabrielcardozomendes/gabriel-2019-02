export default class Series {
    constructor( titulo, anoEstreia, diretor, genero, elenco, temporada, episodios, distribuidora) {
        this.titulo = titulo
        this.anoEstreia = anoEstreia
        this.diretor = diretor
        this.genero = genero
        this.elenco = elenco
        this.temporada = temporada
        this.episodios = episodios
        this.distribuidora = distribuidora
    }
}