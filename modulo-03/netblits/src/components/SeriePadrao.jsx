import React from 'react'
import { Link } from 'react-router-dom';
import '../css/TelaNetBlits.css'
import ListaSeries from '../models/ListaSeries'
const SeriePadrao = props => {
  const { series }  = props
  return(
    <React.Fragment>
    <div className="NetBlitsCss">
        <header className="header">
            <Link className="btnVoltar" to="/">home</Link>
            <Link className="btnNetBlits" to="/mirror">ReactMirror</Link>
        </header>
        <div className="lateral">
          
            <h1>{ series.titulo }</h1>
            <div className="serieAtual">
              <img src={ ListaSeries.img }/>
              <div className="temporadas">Temporadas: {series.temporada}</div>
              <div className="episodios">Episodios: {series.episodios}</div>
            </div>
            <div className="telaPrincialNetBlits">
              <div className="anoEstreia">Ano de Estreia: {series.anoEstreia}</div>
              <div className="diretor">Diretor: {series.diretor}</div>
              <div className="genero">Gênero: {series.genero}</div>
              <div className="elenco">Elenco: {series.elenco}</div>
              <div className="distribuidora">Distribuidora: {series.distribuidora}</div>
            </div>
            <button className="proximaSerie" onClick={ props.btnAumentaIndiceComp() }>Próxima</button>
        </div>
    </div>
</React.Fragment>
  )
}
export default SeriePadrao