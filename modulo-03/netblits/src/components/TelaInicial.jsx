import React, { Component } from 'react';
import '../css/TelaInicial.css'
import { Link } from 'react-router-dom';
import TelaNetBlits from './TelaNetBlits'
import TelaMirror from './TelaMirror'

const TelaInicial = props => {
    const {series} = props
    return(
       <React.Fragment>
           <div className="TelaInicialCss">
            <div>
                <Link className="linkNetBlits" to="/netBlits">NetBlits</Link>
                <Link className="linkReactMirror" to="/reactMirror">React Mirror</Link>
            </div>
           </div>
       </React.Fragment>
    )
}
export default TelaInicial