import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as axios from 'axios';
import '../css/TelaMirror.css';
import ListaMirror from '../models/ListaMirror';
import EpisodioPadrao from '../components/EpisodioMirrorPadrao';
import MensagemFlash from '../components/MensagemFlash';
import MensagemFlashConst from '../constants/MensagemFlashConst';
import MeuInputNumero from '../components/MeuInputNumero';

class ReactMirror extends Component {
  constructor ( props ) {
    super( props )
    this.listaMirror = new ListaMirror();
    this.state = {
      episodio: this.listaMirror.episodiosAleatorios,
      exibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }
  
  sortear() {
    const episodio = this.listaMirror.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido() {
    const { episodio } = this.state
    this.listaMirror.marcarComoAssistido( episodio )
    this.setState( {
      episodio
    } )
  }

  registrarNota( { nota, erro } ) {
    this.setState({
      deveExibirErro: erro
    })
    if ( erro ) {
      return;
    }

    const { episodio } = this.state
    let cor, mensagem
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota )
      cor = 'verde'
      mensagem = MensagemFlashConst.SUCESSO.REGISTRAR_NOTA
    } else {
      cor = 'vermelho'
      mensagem = MensagemFlashConst.ERRO.NOTA_INVALIDA
    }
    this.exibirMensagem( { cor, mensagem } )
  }

  exibirMensagem = ( { cor, mensagem} ) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  logout() {
    localStorage.removeItem('Authorization');
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    return (
      <div className="App">
        <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                       cor={ cor }
                       deveExibirMensagem={ exibirMensagem }
                       mensagem={ mensagem } segundos={ 5 } />
        <div className="App-Header">
           <button type="button" onClick={ this.logout.bind( this ) }>Deslogar</button>
          <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear.bind( this ) } MarcarNoComp={ this.marcarComoAssistido.bind( this ) } />
          <MeuInputNumero placeholder="1 a 5" mensagemCampo="Qual sua nota para esse episódio?" atualizarValor={ this.registrarNota } obrigatorio={ true } deveExibirErro={ deveExibirErro } visivel={ episodio.assistido || false } />
        </div>
      </div>
    );
  }
}

export default ReactMirror;
