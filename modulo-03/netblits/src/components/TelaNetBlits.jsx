import React, {Component} from 'react'
import { Link } from 'react-router-dom';
import '../css/TelaNetBlits.css'
import ListaSeries from '../models/ListaSeries'
import SeriePadrao from './SeriePadrao'

class TelaNetBlits extends Component {
    constructor( props ) {
        super( props )
        this.ListaSeries = new ListaSeries()
        this.state = {
            series: this.ListaSeries.seriePorIndice
        }
    }

    btnAumentaIndice = () => {
        this.ListaSeries.btnAumentaIndice()
        
    }

    render() {
        const { series } = this.state
        return (
          <React.Fragment>
            <SeriePadrao series={ series } btnAumentaIndiceComp={ this.btnAumentaIndice.bind( this ) }/>
          </React.Fragment>
        )
      }
}

export default TelaNetBlits