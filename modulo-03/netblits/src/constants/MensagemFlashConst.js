const Mensagens = {
    SUCESSO: {
        RESGISTAR_NOTA: 'Registramos sua nota'
    },
    ERRO: {
        NOTA_INVALIDA: 'Informar uma nova válida (entre 1 e 5)',
        CAMPO_OBRIGATORIO: '* obrigatório'
    }
}

export default Mensagens