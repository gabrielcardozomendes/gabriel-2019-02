import ListaMirror from './ListaMirror'

export default class ConteudoMsg {
    constructor( conteudo ) {
        this.conteudo = conteudo
    }

    exibirMensagem() {
        if(this.conteudo > 5 || this.conteudo <= 0){
            return false
        }else{
            return true
        }
    }
}