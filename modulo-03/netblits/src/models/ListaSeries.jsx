import Series from '../Series'
import PropTypes from 'prop-types'
import Stranger from '../img/stranger.PNG'
export default class ListaSeries {
    constructor() { 
        this.todas = [
            { titulo:"Stranger Things", anoEstreia:2016, diretor:["Matt Duffer, ","Ross Duffer"], genero:["Suspense, ","Ficcao Cientifica, ","Drama"], elenco:["Winona Ryder, ","David Harbour, ","Finn Wolfhard, ","Millie Bobby Brown, ","Gaten Matarazzo, ","Caleb McLaughlin, ","Natalia Dyer, ","Charlie Heaton, ","Cara Buono, ","Matthew Modine, ","Noah Schnapp"], temporadas:2, numeroEpisodios:17, distribuidora:"Netflix", img: Stranger },
            { titulo:"Game Of Thrones", anoEstreia:2011, diretor:["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"], genero:["Fantasia","Drama"], elenco:["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"], temporadas:7, numeroEpisodios:67, distribuidora:"HBO" },
            { titulo:"The Walking Dead", anoEstreia:2010, diretor:["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"], genero:["Terror","Suspense","Apocalipse Zumbi"], elenco:["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"], temporadas:9, numeroEpisodios:122, distribuidora:"AMC" },
            { titulo:"Band of Brothers", anoEstreia:20001, diretor:["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"], genero:["Guerra"], elenco:["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"], temporadas:1, numeroEpisodios:10, distribuidora:"HBO" },
            { titulo:"The JS Mirror", anoEstreia:2017, diretor:["Lisandro","Jaime","Edgar"], genero:["Terror","Caos","JavaScript"], elenco:["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"], temporadas:1, numeroEpisodios:40, distribuidora:"DBC" },
            { titulo:"10 Days Why", anoEstreia:2010, diretor:["Brendan Eich"], genero:["Caos","JavaScript"], elenco:["Brendan Eich","Bernardo Bosak"], temporadas:10, numeroEpisodios:10, distribuidora:"JS"},{ titulo:"Mr. Robot", anoEstreia:2018, diretor:["Sam Esmail"], genero:["Drama","Techno Thriller","Psychological Thriller"], elenco:["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"], temporadas:3, numeroEpisodios:32, distribuidora:"USA Network" },
            { titulo:"Narcos", anoEstreia:2015, diretor:["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"], genero:["Documentario","Crime","Drama"], elenco:["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"], temporadas:3, numeroEpisodios:30, distribuidora:null },
            { titulo:"Westworld", anoEstreia:2016, diretor:["Athena Wickham"], genero:["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"], elenco:["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"], temporadas:2, numeroEpisodios:20, distribuidora:"HBO" },
            { titulo:"Breaking Bad", anoEstreia:2008, diretor:["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"], genero:["Acao","Suspense","Drama","Crime","Humor Negro"], elenco:["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"], temporadas:5, numeroEpisodios:62, distribuidora:"AMC" }
        ].map( s => new Series( s.titulo, s.anoEstreia, s.diretor, s.genero, s.elenco, s.temporadas, s.numeroEpisodios, s.distribuidora, s.img ) )
    }

    get series() {
        return this.todas
    }

    btnAumentaIndice(){
        return console.log('aqui')
    }

    get img(){
        return this.todas.img
    }

    get seriePorIndice(){
        let indice = 0 
        return this.todas[indice]
    }
    

    hashTagSecreta() {
        console.log('Exercicio 8')
        const serie = new ListaSeries().series
        for (let i = 0; i < serie.length; i++) {
            let e = serie[i].elenco
            let checkA
            e.forEach(nome => {
                if(nome.includes('.')){
                    checkA = true
                }
            })
    
            if(checkA){
                let tag = "#"
                for (let i = 0; i < serie.length; i++) {
                    for (let j = 0; j < serie[i].elenco.length; j++) {
                        let e = serie[i].elenco[j].split(' ').filter(elencoSerie => elencoSerie.includes('.'))

                        serie[i].elenco[0].split(' ')

                        e.forEach(elenco => {
                        tag += elenco.toUpperCase()
                        })
                    }
                }
                return tag
            }
        }
        return null
    }
}

ListaSeries.PropTypes = {
    hashTagSecreta: PropTypes.Array
}

Array.prototype.seriesInvalidas = function() {
    const invalidas = []
    const series = new ListaSeries().series
        for( let i = 0; i < series.length; i++ ) {
            let serie = series[ i ]

            if( serie.distribuidora === null ) {
                invalidas.push( serie )
            }

            if( serie.anoEstreia > 2019 ) {
                invalidas.push( serie )
            }
        }
        console.log('Exercicio 1')
    return invalidas
}

Array.prototype.filtrarPorAno = function( ano ) {
    let validasPorAno = []
    validasPorAno = new ListaSeries().series
    console.log('Exercicio 2')
    return validasPorAno.filter( serie => serie.anoEstreia >= ano )
}

Array.prototype.filtrarPorNome = function( nome ) {
    const serie  = new ListaSeries().series
    console.log('Exercício 3')
    
    for (let i = 0; i < serie.length; i++) {
        for (let j = 0; j < serie[i].elenco.length; j++) {
            let e = serie[i].elenco[j]
            if(e.match(nome)){
                return true;
            }
        }
    }
}

Array.prototype.mediaDeEpisodios = function() {
    const serie  = new ListaSeries().series
    console.log('Exercício 4')
  
    let total = serie.length
    let qtdEps = 0

    for (let i = 0; i < total; i++) {
        qtdEps += serie[i].episodios
    }

    return qtdEps/total
}

Array.prototype.totalSalarios = function( indice ) {
    const serie = new ListaSeries().series
    console.log('Exercício 5')
    let totalDiretores = serie[indice].diretor.length * 100000
    let totalElenco = serie[indice].elenco.length * 40000

    return `R$: ${totalDiretores + totalElenco}`
}

Array.prototype.queroGenero = function( generoSerie ) {
    const serie = new ListaSeries().series
    console.log('Exercício 6')
    let filtradasPorGenero = []

    for (let i = 0; i < serie.length; i++) {
        let g = serie[i].genero
        for (let j = 0; j < g.length; j++) {
            if(g[j] == generoSerie){
                filtradasPorGenero.push(serie[i])
            }
        }
    }

    return filtradasPorGenero
}

function ordenaNomes(nome) {
    return nome.sort(function(x,y) {
        x = x.split(' ')
        y = y.split(' ')

        if( x[x.length -1] < y[y.length -1]){
            return -1
        }
        return 0   
    })
}

Array.prototype.creditos = function( indice ) {
    const serie = new ListaSeries().series[indice]
    let elenco = ordenaNomes(serie.elenco)
    let diretor = ordenaNomes(serie.diretor)
    console.log('Exercício 7')
    return [ elenco, diretor ]
}

/* Array.prototype.hashTagSecreta = function() {
    const serie = new ListaSeries().series
    for (let i = 0; i < serie.length; i++) {
        let e = serie[i].elenco
        let checkA
        e.forEach(nome => {
            if(nome.includes('.')){
                checkA = true
            }
        })

        if(checkA){
            let tag = "#"
            for (let j = 0; j < serie.length; j++) {
                let e = serie[j].split(' ').filter(elencoSerie => elencoSerie.includes('.'))
                e = e[0].split(' ')
                e.forEach(elenco => {
                    tag += elenco.toUpperCase()
                })
            }
            return tag
        }
    }
    return null
} */