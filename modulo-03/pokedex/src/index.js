/* eslint-disable no-unused-vars */
const pokeApi = new PokeApi( )

async function buscar() {
  let pokemonEspecifico = await pokeApi.buscarEspecifico(12)
  let poke = new Pokemon( pokemonEspecifico )
  renderizacaoPokemon( poke )
}

buscar()

function renderizacaoPokemon( pokemon ) {
  document.getElementById( 'idP' ).innerHTML = `PokeID: ${ pokemon.idPoke }`

  document.getElementById( 'nomeP' ).innerHTML = pokemon.nome

  document.getElementById( 'alturaP' ).innerHTML = `Altura: ${ pokemon.conversaoAltura( 10 ) }cm`

  document.getElementById( 'pesoP' ).innerHTML = `Peso: ${ pokemon.peso / 10 }kg`

  document.getElementById( 'tipoP' ).innerHTML = `Tipo: ${ pokemon.tipo }`

  document.getElementById( 'estatisticasP' ).innerHTML = pokemon.estatisticas

  document.getElementById( 'img' ).src = pokemon.img
}

function buscarPokemon( form ) {
  if ( form.id.value === '' || form.id.value <= 0 ) {
    // eslint-disable-next-line no-alert
    alert( 'Digite um ID válido!' )
  } else {
    const pokemonEspecifico = pokeApi.buscarEspecifico( form.id.value )
    pokemonEspecifico.then( pokemon => {
      const poke = new Pokemon( pokemon )
      renderizacaoPokemon( poke )
    } )
  }
}

function pokemonAleatorio() {
  const randNum = Math.floor( Math.random() * ( 802 + 1 ) )
  const pokemonEspecifico = pokeApi.buscarEspecifico( randNum )
  pokemonEspecifico.then( pokemon => {
    const poke = new Pokemon( pokemon )
    document.getElementById( 'id' ).value = poke.idPoke
    renderizacaoPokemon( poke )
  } )
}


