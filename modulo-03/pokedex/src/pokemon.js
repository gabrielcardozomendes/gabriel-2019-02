class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.img = obj.sprites.front_default
    this.nome = obj.name
    this.idPoke = obj.id
    this.altura = obj.height
    this.peso = obj.weight
    this.tipo = obj.types.map( item => item.type.name ).toString()
    this.estatisticas = obj.stats.map( status => `<li> ${ status.stat.name }:  ${ status.base_stat } </li><br>` ).toString().replace( /,/g, '\n' )
  }

  conversaoAltura( multiplicador ) {
    return this.altura * multiplicador
  }
}
