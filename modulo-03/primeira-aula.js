//console.log("Cheguei");

//console.log(teste1);

var teste = "123";
let teste1 = "123";
const test2 = 1222;

//var teste = "111";
//este1 = "222"
//console.log(teste1);

{
    let teste1 = "Aqui mudou";
    //console.log(teste1);
}

//console.log(teste1);

const pessoa = {
    nome: "Gabriel",
    idade: 18,
    endereco: {
        logradouro: "Aquela rua lá",
        numero: 898
    }
}

Object.freeze(pessoa);

pessoa.nome = "Gabriel Luis";

//console.log(pessoa);

function somar(valor1, valor2){
    console.log(valor1 + valor2);
}

//somar(2, 3);

function ondeMoro(cidade){
    //console.log("Eu moro em " + cidade + "E sou muito feliz");
    console.log(`Eu moro em ${cidade} e sou muito feliz`);
}

function quemSou(pessoa){
    //console.log("Eu moro em " + cidade + "E sou muito feliz");
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade} anos`);
}

//quemSou(pessoa);

let funcaoSomarVar = function (a, b, c = 0) {
    return a + b + c;
}

let add = funcaoSomarVar
let resultado = add(3, 2)
//console.log(resultado);


const { nome:n, idade:i } = pessoa
const { endereco: { logradouro, numero } } = pessoa
//console.log(n, i);
//console.log(logradouro, numero);

const array = [1, 3, 4, 8];
const [ n1, ,n2, , n3 = 9 ] = array;
//console.log(n1, n2, n3);

function testarPessoa({ nome, idade }) {
    console.log(nome, idade);
}

//testarPessoa(pessoa);

let a1 = 42;
let b1 = 15;

console.log(a1, b1);

[a1, b1] = [b1, a1];

console.log(a1, b1);