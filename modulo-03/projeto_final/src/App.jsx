import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import { PrivateRoute } from './components/PrivateRoute'
import PaginaInicial from './components/PaginaInicial'
import Login from './components/Login'
import PaginaAgencia from './components/PaginaAgencia'
import PaginaClientes from './components/PaginaClientes'
import PaginaTipoContas from './components/PaginaTipoContas'
import PaginaContasClientes from './components/PaginaContasClientes'

export default class App extends Component {
  render(){
    return (
      <Router>
        <React.Fragment>
          <PrivateRoute path="/" exact component={ PaginaInicial }/>
          <PrivateRoute path="/agencia" exact component={ PaginaAgencia }/>
          <PrivateRoute path="/clientes" exact component={ PaginaClientes }/>
          <PrivateRoute path="/tipoContas" exact component={ PaginaTipoContas }/>
          <PrivateRoute path="/contasClientes" exact component={ PaginaContasClientes }/>
          <Route path="/login" component={ Login } />
        </React.Fragment>
      </Router>
    );
  }
}