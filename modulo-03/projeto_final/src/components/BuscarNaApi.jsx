import * as axios from 'axios'

export default class BuscarNaApi {
    constructor() {
        this.obj = []
        this.baseUrl = 'http://localhost:1337'
    }

    buscar( url, id ) {
        url += id ? id : ""
        axios.get(`${ this.baseUrl }/${ url }`,{
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        }).then( resp => {
            this.obj = resp.data
            console.log(this.obj)
        }).catch( function ( error ){
            console.log("error")
        })
    }

    buscarListaAgencias() {
        this.buscar('agencias')
    }

    buscarListaClientes() {
        this.buscar('clientes')
    }

    buscarListaTipos() {
        this.buscar('tipoContas')
    }

    buscarContasDeClientes(){
        this.buscar('conta/clientes');
    }

    get buscarAgencias(){
        return this.obj.agencias
    }

    get buscarClientes() {
        return this.obj.clientes
    }

    get buscarTipos() {
        return this.obj.tipos
    }

    get buscarContasClientes() {
        return this.obj.cliente_x_conta
    }
}