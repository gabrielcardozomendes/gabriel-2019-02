import React, { Component } from 'react'
import BuscarNaApi from '../components/BuscarNaApi'

export default class InformacoesAgencia extends Component {
  constructor( props ) {
    super( props )
    this.api = new BuscarNaApi()
    this.state = {
      ListaAgencias: this.api.buscarAgencias
    }
  }

  componentWillMount() {
    this.api.buscarListaAgencias()
        setTimeout( () => {
            this.setState({
                ListaAgencias: this.api.buscarAgencias
            })
        },2000)
  }

  render() {

  }
}