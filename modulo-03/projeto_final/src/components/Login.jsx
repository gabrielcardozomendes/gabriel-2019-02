import React, { Component } from 'react'
import * as axios from 'axios'
import '../css/Login.css'
import Logo from '../img/Logo.png'

export default class Login extends Component {
    constructor( props ) {
        super( props )
        
        this.state = {
            email: '',
            senha: ''
        }
        this.trocaState = this.trocaState.bind( this )
    }

    trocaState ( evt ) {
        const { name, value } = evt.target

        this.setState ( {
            [name]: value
        } ) 
    }

    logar ( evt ) {
        evt.preventDefault()

        const { email, senha } = this.state

        if( email && senha ) {
            axios.post( 'http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha
            } ).then( resp => {
                localStorage.setItem( 'Authorization', resp.data.token )
                this.props.history.push( "/" )
            } )
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="cardLogar">
                        <img className="logo" src={ Logo }/>
                    <div className="inputs">
                        <label className="labelEmail">E-Mail</label>
                        <input type="text" className="email" name="email" id="email" placeholder="Email" onChange={ this.trocaState }/>
                        <label className="labelSenha">Senha</label>
                        <input type="password" className="senha" name="senha" id="senha" placeholder="Senha" onChange={ this.trocaState }/>
                        <button type="button" className="btnLogar" onClick={ this.logar.bind( this ) }>Logar</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}