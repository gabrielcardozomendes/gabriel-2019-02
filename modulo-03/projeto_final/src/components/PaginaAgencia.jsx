import React, { Component } from 'react'
import '../css/PaginaAgencias.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Logo from '../img/Logo.png'
import BuscarNaApi from '../components/BuscarNaApi'
import Loading from '../img/loading.gif'

export default class PaginaAgencia extends Component {
    constructor( props ) {
        super( props )
        this.api = new BuscarNaApi()
        this.state = {
            ListaAgencias: this.api.buscarAgencias
        }
    }

    componentWillMount(){
        this.api.buscarListaAgencias()
        setTimeout( () => {
            this.setState({
                ListaAgencias: this.api.buscarAgencias
            })
        },2000)
    }

    logout() {
        localStorage.removeItem('Authorization')
    }

    render() {
        const { ListaAgencias } = this.state
        
        return (
            <React.Fragment>
                <header className="headerAgencia">
                    <Link to="/"><img className="logo" src={ Logo }/></Link>
                    <h5 className="titulo">Agências</h5>
                    <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                </header>
                { ListaAgencias ? ListaAgencias.map( agencias => {
                   return(
                       <React.Fragment className="itensAgencia">
                            <div className="cardAgencia">
                                <h3>Agência: {agencias.nome}</h3>
                                <h5>ID: {agencias.id}</h5>
                            </div>
                       </React.Fragment>
                   )
               })
               :
               <div>
                   <img className="loading" src={Loading}/>
               </div>
            }
            </React.Fragment>
        )
    }
}