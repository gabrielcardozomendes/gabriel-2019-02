import React, { Component } from 'react'
import '../css/PaginaClientes.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Logo from '../img/Logo.png'
import BuscarNaApi from '../components/BuscarNaApi'
import Loading from '../img/loading.gif'

export default class PaginaAgencia extends Component {
    constructor( props ) {
        super( props )
        this.api = new BuscarNaApi()
        this.state = {
            ListaClientes: this.api.buscarClientes
        }
    }

    componentWillMount() {
        this.api.buscarListaClientes()
        setTimeout( () => {
            this.setState({
                ListaClientes: this.api.buscarClientes
            })
        },2000)
    }

    logout() {
        localStorage.removeItem('Authorization')
    }

    render() {
        const { ListaClientes } = this.state
        return (
            <React.Fragment>
                <header className="headerClientes">
                <Link to="/"><img className="logo" src={ Logo }/></Link>
                    <h5 className="titulo">Clientes</h5>
                    <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                </header>
                { ListaClientes ? ListaClientes.map( clientes => {
                   return(
                       <React.Fragment className="itensClientes"> 
                            <div className="cardAgencia">
                                <h3>Nome: {clientes.nome}</h3>
                                <h5>ID: {clientes.id}</h5>
                            </div>
                       </React.Fragment>
                   )
               })
               :
               <div>
                   <img className="loading" src={Loading}/>
               </div>
            }
            </React.Fragment>
        )
    }
}