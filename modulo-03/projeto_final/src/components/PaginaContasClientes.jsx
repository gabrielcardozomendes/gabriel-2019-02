import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import '../css/PaginaClientes.css'
import Logo from '../img/Logo.png'
import BuscarNaApi from '../components/BuscarNaApi'
import Loading from '../img/loading.gif'

export default class PaginaAgencia extends Component {
    constructor( props ) {
        super( props )
        this.api = new BuscarNaApi()
        this.state = {
            ListaContaClientes: this.api.buscarContasClientes
        }
    }

    componentWillMount() {
        this.api.buscarContasDeClientes()
        setTimeout( () => {
            this.setState({
                ListaContaClientes: this.api.buscarContasClientes
            })
        },2000)
    }

    logout() {
        localStorage.removeItem('Authorization')
    }

    render() {
        const { ListaContaClientes } = this.state
        return (
            <React.Fragment>
                <header className="headerClientes">
                <Link to="/"><img className="logo" src={ Logo }/></Link>
                    <h5 className="titulo">Contas de Clientes</h5>
                    <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                </header>
                { ListaContaClientes ? ListaContaClientes.map( contaClientes => {
                   return(
                       <React.Fragment className="itensClientes"> 
                            <div className="cardAgencia">
                                <h3>Código: {contaClientes.nome}</h3>
                                <h5>ID: {contaClientes.id}</h5>
                            </div>
                       </React.Fragment>
                   )
               })
               :
               <div>
                   <img className="loading" src={Loading}/>
               </div>
            }
            </React.Fragment>
        )
    }
}