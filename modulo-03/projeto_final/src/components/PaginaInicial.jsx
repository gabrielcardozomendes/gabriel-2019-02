import React, { Component } from 'react'
import '../css/PaginaInicial.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Logo from '../img/Logo.png'
import ImgAgencias from '../img/Agencias.png'
import ImgClientes from '../img/Clientes.png'
import ImgTipos from '../img/TiposDeContas.png'
import ImgContasClientes from '../img/ContasDeClientes.png'
export default class PaginaInicial extends Component {
    constructor( props ) {
        super( props )
    }

    logout() {
        localStorage.removeItem('Authorization')
    }

    render() {
        return (
            <React.Fragment>
                <header className="headerPagina">
                    <Link to="/"><img className="logo" src={ Logo }/></Link>
                    <h5 className="titulo">Página Inicial</h5>
                    <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                </header>
                <body>
                    <div className="itensInicial">
                            <Link to="/agencia" type="button" className="agencias"><img className="agenciasImg" src={ImgAgencias}/></Link>
                            <Link to="/clientes" type="button" className="clientes"><img className="clientesImg" src={ImgClientes}/></Link>
                            <Link to="/tipoContas" type="button" className="tiposContas"><img className="tipoContasImg" src={ImgTipos}/></Link>
                            <Link to="/contasClientes" type="button" className="contaClientes"><img src={ImgContasClientes}/></Link>
                    </div>
                </body>
            </React.Fragment>
        )
    }
}