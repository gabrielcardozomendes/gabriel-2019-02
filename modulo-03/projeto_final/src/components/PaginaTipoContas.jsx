import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import '../css/PaginaTipoContas.css'
import BuscarNaApi from '../components/BuscarNaApi'
import Logo from '../img/Logo.png'
import Loading from '../img/loading.gif'


export default class TiposContas extends Component {
    constructor( props ) {
        super( props )
        this.api = new BuscarNaApi()
        this.state = {
            ListaTipoContas: this.api.buscarTipos
        }
    }

    componentWillMount() {
        this.api.buscarListaTipos()
        setTimeout( () => {
            this.setState({
                ListaTipoContas: this.api.buscarTipos
            })
        },2000)
    }

    render() {
        const { ListaTipoContas } = this.state
        return (
            <React.Fragment>
                <header className="headerTipoContas">
                <Link to="/"><img className="logo" src={ Logo }/></Link>
                    <h5 className="titulo">Tipos de Contas</h5>
                    <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                </header>
                { ListaTipoContas ? ListaTipoContas.map( TipoContas => {
                   return(
                       <React.Fragment className="itensTipoContas">
                            <div className="cardTipoContas">
                                <h3>{ TipoContas.nome }</h3>
                                <h5>ID: { TipoContas.id }</h5>
                            </div>
                       </React.Fragment>
                   )
               })
               :
               <div>
                   <img className="loading" src={Loading}/>
               </div>
            }
            </React.Fragment>
        )
    }
}