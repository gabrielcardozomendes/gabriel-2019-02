import React, { Component } from 'react'
import './App.css'
import ListaEpisodios from './models/ListaEpisodios'
import EpisodioPadrao from './components/EpisodioPadrao'
import TesteRenderizacao from './components/TesteRenderizacao'

class App extends Component {
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({ 
      episodio 
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio
    } )
  }
  
  definirNotaEP() {
    const { episodio } = this.state
    this.listaEpisodios.definirNotaEP()
    this.setState( { 
      episodio,
      exibirMensagem: true
    } )
    setTimeout( () => {
      this.setState( { 
        exibirMensagem: false
      } )
    }, 5000 )
  }

  gerarCampoNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && ( 
            <div>
              <span>Qual sua nota para este episódio ? </span>
              <input type="number" id="inputNota" placeholder="1 a 5" onBlur={ this.definirNotaEP.bind( this ) }></input>
              <button onClick={ () => this.definirNotaEP() }> Enviar </button>
            </div>
          )
        }
      </div>
    )
  }

  render() {
    const { episodio, exibirMensagem } = this.state
    return (
      <div className="App">
        <div className="App-Header">
          {/* <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear.bind( this ) } marcarNoComp={ this.marcarComoAssistido.bind( this ) }/>
          { this.gerarCampoNota() }
          <h5>{ exibirMensagem ? 'Nota registrada com sucesso' : '' }</h5> */}
          <TesteRenderizacao /* nome={ 'Gabriel' } */>
            <h4>Aqui novamente</h4>
          </TesteRenderizacao>
        </div>
      </div>
    )
  }
}

export default App
