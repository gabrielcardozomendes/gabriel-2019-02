import React from 'react'
const EpisodioPadrao = props =>{
   const {episodio} = props
   return(
       <React.Fragment>
        <button onClick={ () => props.sortearNoComp() /* .bind( this ) */ }>Próximo</button>
        <button onClick={ () => props.marcarNoComp() /* .bind( this ) */ }>Já vi</button>
         <h2>{ episodio.nome}</h2>
         <img src= { episodio.thumbUrl } alt={ episodio.nome }></img>
         <h4> duração {episodio.duracaoEmMin}</h4>
         <h4> Temporada/Episódio {episodio.temporadaEpisodio}</h4>
         <h4> Já Assisti? {episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es)</h4>
         <h4>{(episodio.nota || 'Sem nota')}</h4>
       </React.Fragment>
   )
}
export default EpisodioPadrao