package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.AbstractEntity;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.AbstractService;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class AbstractController < Entidade extends AbstractEntity,
                                           EntidadeService extends AbstractService< Entidade, EntidadeRepository >,
                                           EntidadeRepository extends CrudRepository< Entidade, Integer >
                                           > {
    @Autowired
    EntidadeService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<Entidade> listAll() throws Exception {
        return service.all();
    }

    @PostMapping( value = "/new" )
    @ResponseBody
    public Entidade newEntity( @RequestBody Entidade entidade ) throws Exception {
        return service.save( entidade );
    }

    @PutMapping( value = "/edit/{id}")
    @ResponseBody
    public Entidade edit( @PathVariable Integer id, @RequestBody Entidade entidade ) throws Exception {
        return service.edit( id, entidade );
    }

    @DeleteMapping( value = "delete/{id}" )
    @ResponseBody
    public void delete( @PathVariable Integer id ) throws Exception {
        service.remove( id );
    }
}
