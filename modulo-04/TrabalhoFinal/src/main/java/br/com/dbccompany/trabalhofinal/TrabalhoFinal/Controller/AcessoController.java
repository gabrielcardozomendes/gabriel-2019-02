package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Acesso;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.AcessoRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/acesso")
public class AcessoController extends AbstractController<Acesso, AcessoService, AcessoRepository> {}
