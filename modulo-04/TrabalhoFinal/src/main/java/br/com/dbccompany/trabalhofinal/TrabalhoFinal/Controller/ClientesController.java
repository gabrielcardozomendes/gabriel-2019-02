package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ClientesRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/clientes")
public class ClientesController extends AbstractController<Clientes, ClientesService, ClientesRepository> {}
