package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.ClientesPacotes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ClientesPacotesRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/clientesPacotes" )
public class ClientesPacotesController extends AbstractController<ClientesPacotes, ClientesPacotesService, ClientesPacotesRepository> {}
