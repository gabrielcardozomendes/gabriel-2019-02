package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Contato;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ContatoRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contato" )
public class ContatoController extends AbstractController<Contato, ContatoService, ContatoRepository> {}
