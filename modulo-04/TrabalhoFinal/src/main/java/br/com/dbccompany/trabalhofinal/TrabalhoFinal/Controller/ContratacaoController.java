package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Contratacao;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ContratacaoRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contratacao" )
public class ContratacaoController extends AbstractController<Contratacao, ContratacaoService, ContratacaoRepository> {}
