package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.EspacoPacotes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.EspacosPacotesRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.EspacoPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/espacoPacotes")
public class EspacoPacotesController extends AbstractController<EspacoPacotes, EspacoPacotesService, EspacosPacotesRepository> {}
