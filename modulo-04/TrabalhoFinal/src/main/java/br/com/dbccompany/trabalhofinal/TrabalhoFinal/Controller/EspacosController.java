package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.EspacosRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/espacos" )
public class EspacosController extends AbstractController<Espacos, EspacosService, EspacosRepository> {}
