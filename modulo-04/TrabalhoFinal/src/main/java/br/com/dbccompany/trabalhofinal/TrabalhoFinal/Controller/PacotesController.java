package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Pacotes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.PacotesRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pacotes" )
public class PacotesController extends AbstractController<Pacotes, PacotesService, PacotesRepository> {}
