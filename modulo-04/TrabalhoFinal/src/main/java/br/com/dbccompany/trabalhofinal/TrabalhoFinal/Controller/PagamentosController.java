package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Pagamentos;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.PagamentosRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.PagamentosService;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pagamentos")
public class PagamentosController extends AbstractController<Pagamentos, PagamentosService, PagamentosRepository> {}
