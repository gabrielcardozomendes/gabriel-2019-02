package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.SaldoClienteId;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ClientesRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.EspacosRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/saldoCliente" )
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @Autowired
    ClientesRepository clientesRepository;

    @Autowired
    EspacosRepository espacosRepository;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<SaldoCliente> all() {
        return service.all();
    }

//    @GetMapping( value = "/{idCliente}-{idEspaco}" )
//    @ResponseBody
//    public SaldoCliente searchById( @PathVariable Integer idCliente, @PathVariable Integer idEspaco ) {
//
//    }

    @PostMapping( value = "/new" )
    @ResponseBody
    public SaldoCliente save( @RequestBody SaldoCliente saldoCliente ) throws Exception {
        return (SaldoCliente) service.save( saldoCliente );
    }

    @PutMapping( value = "/edit/{idCliente}-{idEspaco}" )
    @ResponseBody
    public SaldoCliente edit( @PathVariable Integer idCliente, @PathVariable Integer idEspaco ,@RequestBody SaldoCliente saldoCliente ) throws Exception {
        return service.edit( saldoCliente, idCliente, idEspaco );
    }

    @DeleteMapping( value = "/delete/{idCliente}-{idEspaco}" )
    @ResponseBody
    public void delete( @PathVariable Integer idCliente, @PathVariable Integer idEspaco ) throws Exception {
        Clientes clientesBanco = clientesRepository.findById(idCliente).get();
        Espacos espacosBanco = espacosRepository.findById(idEspaco).get();

        SaldoClienteId saldoId = new SaldoClienteId(clientesBanco, espacosBanco);

        SaldoCliente saldoCliente = new SaldoCliente();

        saldoCliente.setId(saldoId);

        service.remove( saldoCliente );
    }
}
