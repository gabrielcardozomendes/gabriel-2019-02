package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.TipoContato;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.TipoContatoRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/tipoContato" )
public class TipoContatoController extends AbstractController<TipoContato, TipoContatoService, TipoContatoRepository> {}
