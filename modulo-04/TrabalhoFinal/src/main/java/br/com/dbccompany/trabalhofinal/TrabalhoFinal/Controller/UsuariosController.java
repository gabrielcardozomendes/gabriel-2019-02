package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Controller;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.UsuariosRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/usuarios" )
public class UsuariosController extends AbstractController<Usuarios, UsuariosService, UsuariosRepository> {}
