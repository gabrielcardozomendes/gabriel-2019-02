package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.Date;


@Entity
@SequenceGenerator( allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ" )
public class Acesso extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumns({
            @JoinColumn( name = "id_cliente_saldo_clinte", referencedColumnName = "id_cliente" ),
            @JoinColumn( name = "id_espaco_saldo_cliente", referencedColumnName = "id_espaco")
    })
    private SaldoCliente saldoCliente;

    @Column( name = "IS_ENTRADA" )
    private boolean isEntrada;

    @Column( name = "DATA" )
    private Date data;

    @Column( name = "IS_EXCECAO" )
    private boolean isExcecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
