package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
public class Clientes extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "CPF", nullable = false, unique = true )
    private String cpf;

    @Column( name = "DATA_NASCIMENTO", nullable = false )
    private Date dataNascimento;

    @ManyToOne
    @JoinColumn( name = "fk_contato" )
    private Contato contato;

    @OneToMany( mappedBy = "clientes", cascade = CascadeType.MERGE )
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "clientes", cascade = CascadeType.MERGE )
    private List<Contratacao> contratacao = new ArrayList<>();

    @OneToMany( mappedBy = "clientes", cascade = CascadeType.MERGE )
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }
}
