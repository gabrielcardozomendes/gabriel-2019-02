package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ")
public class ClientesPacotes extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "QUANTIDADE" )
    private Integer quantidade;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name  = "fk_cliente" )
    private Clientes clientes;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name  = "fk_pacotes" )
    private Pacotes pacotes;

    @OneToMany( mappedBy = "clientesPacotes", cascade = CascadeType.MERGE )
    private List<Pagamentos> pagamentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
