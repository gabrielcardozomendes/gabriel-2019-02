package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ" )
public class Contato extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "VALOR" )
    private String valor;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "fk_tipo_contato" )
    private TipoContato tipoContato;

    @OneToMany( mappedBy = "contato" )
    private List<Clientes> clientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }
}
