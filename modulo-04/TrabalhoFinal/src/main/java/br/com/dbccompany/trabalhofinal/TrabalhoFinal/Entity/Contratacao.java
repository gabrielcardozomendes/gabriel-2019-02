package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Enum.TipoContratacao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
public class Contratacao extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy =  GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "TIPO_CONTRATACAO" )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE" )
    private Integer quantidade;

    @Column( name = "DESCONTO" )
    private Double desconto;

    @Column( name = "PRAZO" )
    private int prazo;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_espaco" )
    private Espacos espacos;

    @OneToMany( mappedBy = "contratacao", cascade = CascadeType.MERGE )
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_cliente" )
    private Clientes clientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
