package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Enum.TipoContratacao;

import javax.persistence.*;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ESPACO_PACOTES_SEQ", sequenceName = "ESPACO_PACOTES_SEQ" )
public class EspacoPacotes extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "ESPACO_PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "TIPO_CONTRATACAO" )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE" )
    private Integer quantidade;

    @Column( name = "PRAZO" )
    private int prazo;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_espaco" )
    private Espacos espacos;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_pacotes" )
    private Pacotes pacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }
}
