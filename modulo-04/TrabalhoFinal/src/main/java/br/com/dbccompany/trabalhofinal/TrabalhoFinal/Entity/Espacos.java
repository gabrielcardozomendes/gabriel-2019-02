package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ" )
public class Espacos extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "NOME", nullable = false, unique = true )
    private String nome;

    @Column( name = "QTDPESSOAS", nullable = false )
    private int qtdPessoas;

    @Column( name = "VALOR", nullable = false )
    private Integer valor;

    @OneToMany( mappedBy = "espacos", cascade = CascadeType.MERGE )
    private List<EspacoPacotes> espacoPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "espacos", cascade = CascadeType.MERGE )
    private List<Contratacao> contratacao = new ArrayList<>();

    @OneToMany( mappedBy = "espacos", cascade = CascadeType.MERGE )
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public List<EspacoPacotes> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<EspacoPacotes> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }
}
