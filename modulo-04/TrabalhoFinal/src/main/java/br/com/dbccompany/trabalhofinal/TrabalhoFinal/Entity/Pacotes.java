package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ" )
public class Pacotes extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "PACOTES" )
    private Double pacotes;

    @OneToMany( mappedBy = "pacotes", cascade = CascadeType.MERGE )
    private List<EspacoPacotes> espacoPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "pacotes", cascade = CascadeType.MERGE )
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPacotes() {
        return pacotes;
    }

    public void setPacotes(Double pacotes) {
        this.pacotes = pacotes;
    }

    public List<EspacoPacotes> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<EspacoPacotes> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
