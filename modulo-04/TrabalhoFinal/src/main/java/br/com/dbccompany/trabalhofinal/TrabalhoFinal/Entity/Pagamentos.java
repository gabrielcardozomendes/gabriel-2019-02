package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Enum.TipoPagamento;

import javax.persistence.*;

@Entity
@SequenceGenerator( allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
public class Pagamentos extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Enumerated
    @Column( name = "TIPO_PAGAMENTO" )
    private TipoPagamento tipoPagamento;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_cliente_pacote" )
    private ClientesPacotes clientesPacotes;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_contratacao" )
    private Contratacao contratacao;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }
}
