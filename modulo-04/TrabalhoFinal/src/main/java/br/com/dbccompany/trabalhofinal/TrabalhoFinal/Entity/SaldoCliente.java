package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Enum.TipoContratacao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class SaldoCliente {

    @EmbeddedId
    private SaldoClienteId id;

    @Column( name = "TIPO_CONTRATACAO" )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE" )
    private Integer quantidade;

    @Column( name = "VENCIMENTO" )
    private Date vencimento;

    @OneToMany( mappedBy = "saldoCliente", cascade = CascadeType.MERGE )
    private List<Acesso> acessos = new ArrayList<>();

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_espaco", insertable = false, updatable = false )
    private Espacos espacos;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_cliente", insertable = false, updatable = false )
    private Clientes clientes;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acesso> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acesso> acessos) {
        this.acessos = acessos;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }
}
