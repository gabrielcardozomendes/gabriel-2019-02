package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ" )
public class TipoContato extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "NOME")
    private String nome;

    @OneToMany( mappedBy = "tipoContato", cascade = CascadeType.MERGE )
    private List<Contato> contatos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }
}
