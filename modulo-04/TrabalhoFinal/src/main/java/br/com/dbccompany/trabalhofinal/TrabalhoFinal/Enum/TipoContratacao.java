package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Enum;

public enum TipoContratacao {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES, ESPECIAL;
}
