package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Enum;

public enum TipoPagamento {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA;
}
