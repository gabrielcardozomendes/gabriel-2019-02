package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientesPacotesRepository extends CrudRepository< ClientesPacotes, Integer > {
    List<ClientesPacotes> findAll();

    ClientesPacotes findById( long id );
}
