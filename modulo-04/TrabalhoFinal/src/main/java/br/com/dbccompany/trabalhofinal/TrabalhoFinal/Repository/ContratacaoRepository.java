package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository< Contratacao, Integer > {
    List<Contratacao> findAll();

    Contratacao findById( long id );
}
