package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.EspacoPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosPacotesRepository extends CrudRepository< EspacoPacotes, Integer > {
    List<EspacoPacotes> findAll();

    EspacoPacotes findById( long id );
}
