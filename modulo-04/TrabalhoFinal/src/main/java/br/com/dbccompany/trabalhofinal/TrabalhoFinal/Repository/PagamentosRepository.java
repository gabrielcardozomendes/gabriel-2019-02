package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentosRepository extends CrudRepository< Pagamentos, Integer > {
    List<Pagamentos> findAll();

    Pagamentos findById( long id );
}
