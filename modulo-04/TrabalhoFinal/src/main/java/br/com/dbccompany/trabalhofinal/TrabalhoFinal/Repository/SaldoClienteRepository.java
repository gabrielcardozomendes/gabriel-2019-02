package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository< SaldoCliente, Integer > {
    List<SaldoCliente> findAll();

    SaldoCliente findById( SaldoClienteId id );
}
