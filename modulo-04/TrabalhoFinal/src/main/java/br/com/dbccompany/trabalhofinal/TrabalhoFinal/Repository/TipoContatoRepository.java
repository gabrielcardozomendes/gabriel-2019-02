package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository< TipoContato, Integer > {
    List<TipoContato> findAll();

    TipoContato findById( long id );
}
