package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.AbstractEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class AbstractService < Entidade extends AbstractEntity, Repository extends CrudRepository< Entidade, Integer >> {

    @Autowired
    Repository repository;

    @Transactional( rollbackFor = Exception.class )
    public Entidade save( Entidade entidade ) throws Exception {
        return repository.save( entidade );
    }

    @Transactional( rollbackFor = Exception.class )
    public Entidade edit( Integer id, Entidade entidade ) throws Exception {
        entidade.setId( id );
        return repository.save( entidade );
    }

    @Transactional( rollbackFor =  Exception.class )
    public List<Entidade> all() {
        return (List<Entidade>) repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public void remove( Integer id ) throws  Exception {
        repository.deleteById( id );
    }
}
