package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Acesso;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.AcessoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class AcessoService extends AbstractService<Acesso, AcessoRepository> {

    @Override
    public Acesso save( Acesso acesso ) throws Exception {

        if(acesso.isEntrada()){
            acesso.setEntrada( true );
        }

        if(acesso.getData() == null){
            acesso.setData(new Date(System.currentTimeMillis()));
        }

        if(acesso.getSaldoCliente().getQuantidade() == 0){
            throw new Exception( "Ta zerado.." );
        }

        return super.save( acesso );
    }
}
