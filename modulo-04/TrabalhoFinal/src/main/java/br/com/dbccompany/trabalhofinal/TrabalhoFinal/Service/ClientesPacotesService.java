package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.ClientesPacotes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ClientesPacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientesPacotesService extends AbstractService<ClientesPacotes, ClientesPacotesRepository> {
}
