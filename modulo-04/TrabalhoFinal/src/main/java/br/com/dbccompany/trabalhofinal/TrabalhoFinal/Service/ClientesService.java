package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ClientesRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Utils.Validators;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ClientesService extends AbstractService<Clientes, ClientesRepository> {

    @Override
    @Transactional( rollbackFor = Exception.class )
    public Clientes save( Clientes cliente ) throws Exception {
        if( !Validators.cpfValidator( cliente.getCpf() ) ) {
            throw new Exception( "CPF inválido." );
        }

        return super.save( cliente );
    }
}
