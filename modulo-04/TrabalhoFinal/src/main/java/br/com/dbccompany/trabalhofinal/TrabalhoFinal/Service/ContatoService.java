package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;


import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Contato;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ContatoRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Utils.Validators;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContatoService extends AbstractService<Contato, ContatoRepository> {

    @Override
    @Transactional( rollbackFor = Exception.class )
    public Contato save( Contato contato ) throws Exception {
        if( contato.getTipoContato() != null ) {
            if( !Validators.tipoContatoValidator( contato ) ) {
                throw new Exception( "Tipo contado inválido." );
            }
        } else {
            throw new Exception( "Tipo contado nulo." );
        }

        return super.save( contato );
    }
}
