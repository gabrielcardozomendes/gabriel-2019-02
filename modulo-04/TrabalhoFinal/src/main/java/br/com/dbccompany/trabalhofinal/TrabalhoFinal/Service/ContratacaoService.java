package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Contratacao;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ContratacaoRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Utils.ParseTo;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class ContratacaoService extends AbstractService<Contratacao, ContratacaoRepository> {

    @Transactional
    public String valorDoContrato( Contratacao contratacao ) {

        ParseTo parseTo = new ParseTo();

        double valorContrato = 0.0;

        if( contratacao.getTipoContratacao() != null ) {
            if( contratacao.getEspacos() != null ) {
                double diaria = contratacao.getEspacos().getValor();

            switch ( contratacao.getTipoContratacao() ) {
                case MINUTO:
                    valorContrato = diaria / 1440;
                    break;

                case HORA:
                    valorContrato = diaria / 24 ;
                    break;

                case TURNO:
                    valorContrato = diaria * 5 / 24;
                    break;

                case DIARIA:
                    valorContrato = diaria;
                    break;

                case SEMANA:
                    valorContrato = diaria * 7;
                    break;

                case MES:
                    valorContrato = diaria * 30;
                    break;

                case ESPECIAL:
                    System.out.println("desafio");
                    break;

                default:
                    System.err.println("Escolha um Tipo Contratação válido.");
                }
            }
        }

        double desconto = ( (valorContrato * contratacao.getQuantidade()) - contratacao.getDesconto() );

        String formatar = "Valor total: " + parseTo.formatMoney( desconto );

        return formatar;
    }
}

