package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.EspacoPacotes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.EspacosPacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacoPacotesService extends AbstractService<EspacoPacotes, EspacosPacotesRepository> {
}
