package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.EspacosRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacosService extends AbstractService<Espacos, EspacosRepository> {
}
