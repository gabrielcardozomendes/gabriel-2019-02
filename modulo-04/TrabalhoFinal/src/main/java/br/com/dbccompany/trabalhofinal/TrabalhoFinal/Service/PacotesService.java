package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Pacotes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.PacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class PacotesService extends AbstractService<Pacotes, PacotesRepository> {
}
