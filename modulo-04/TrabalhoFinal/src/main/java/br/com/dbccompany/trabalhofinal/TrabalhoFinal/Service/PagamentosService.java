package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Pagamentos;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.PagamentosRepository;
import org.springframework.stereotype.Service;

@Service
public class PagamentosService extends AbstractService<Pagamentos, PagamentosRepository> {

    @Override
    public Pagamentos save( Pagamentos pagamentos ) throws Exception {
        if( pagamentos.getContratacao() == null ) {
            throw new Exception( "Contratação inválida" );
        } else if( pagamentos.getTipoPagamento() == null ) {
            throw new Exception( "Tipo pagamento inválido." );
        }

        return super.save( pagamentos );
    }
}
