package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.SaldoClienteId;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.ClientesRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.EspacosRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    SaldoClienteRepository repository;

    @Autowired
    ClientesRepository clientesRepository;

    @Autowired
    EspacosRepository espacosRepository;

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente save( SaldoCliente saldoCliente ) throws Exception {
        return repository.save( saldoCliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente edit(SaldoCliente saldoCliente, Integer idEspaco, Integer idCliente ) throws Exception {
        Clientes clientesBanco = clientesRepository.findById(idCliente).get();
        Espacos espacosBanco = espacosRepository.findById(idEspaco).get();

        SaldoClienteId saldoId = new SaldoClienteId(clientesBanco, espacosBanco);

        saldoCliente.setId(saldoId);

        return repository.save( saldoCliente );
    }

    @Transactional( rollbackFor =  Exception.class )
    public List<SaldoCliente> all() {
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public void remove( SaldoCliente saldoClienteId ) throws  Exception {
        repository.delete( saldoClienteId );
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente searchById(@PathVariable Integer idCliente, @PathVariable Integer idEspaco ) {
        Clientes clientesBanco = clientesRepository.findById(idCliente).get();
        Espacos espacosBanco = espacosRepository.findById(idEspaco).get();

        SaldoClienteId saldoId = new SaldoClienteId(clientesBanco, espacosBanco);

        return repository.findById( saldoId );
    }

}
