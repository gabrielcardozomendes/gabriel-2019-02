package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.TipoContato;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends AbstractService<TipoContato, TipoContatoRepository> {
}
