package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Service;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Repository.UsuariosRepository;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Utils.Cryptographer;
import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Utils.Validators;
import org.springframework.stereotype.Service;

@Service
public class UsuariosService extends AbstractService<Usuarios, UsuariosRepository> {

    @Override
    public Usuarios save( Usuarios usuario ) throws  Exception {

        if( !Validators.emailValidator( usuario.getEmail() ) ) {
            throw new Exception( "Email inválido." );
        }

        if( usuario.getSenha().length() < 6 ) {
            throw new Exception( "Tamanho mínimo para o campo senha é de 6 dígitos." );
        }

        Cryptographer.criptograph( usuario.getSenha() );

        return super.save( usuario );
    }
}
