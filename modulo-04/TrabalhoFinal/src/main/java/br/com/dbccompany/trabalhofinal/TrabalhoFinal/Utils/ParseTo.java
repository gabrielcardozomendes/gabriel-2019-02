package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Utils;

import javax.swing.text.NumberFormatter;
import java.text.NumberFormat;
import java.util.Locale;

public class ParseTo {

    public String formatMoney( Double valor ) {
        NumberFormat formatTo = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        return formatTo.format( valor );
    }

    public String noSimbols( String valor ) {
        return  valor.replaceFirst("(R\\$)\\s*", "");
    }

    public String noSeparators( String valor ) {
         String formated =  valor.replaceAll(".", "");
         return formated = valor.replaceAll(",", "");
    }

    public Double toDouble( String valor ) {
        Double valorEsperado = 0.0;

        this.noSeparators( valor );
        this.noSimbols( valor );

        try {
            valorEsperado = Double.parseDouble( valor );
        } catch ( NumberFormatException e ) {
            System.err.println( "Deu ruim pra formatar o double." );
        }

        return valorEsperado;
    }
}
