package br.com.dbccompany.trabalhofinal.TrabalhoFinal.Utils;

import br.com.dbccompany.trabalhofinal.TrabalhoFinal.Entity.Contato;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {

    public static boolean emailValidator( String email ) {
        boolean isValid = false;

        if (email != null && email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

            Pattern pattern = Pattern.compile( expression, Pattern.CASE_INSENSITIVE );

            Matcher matcher = pattern.matcher( email );

            if (matcher.matches()) {
                isValid = true;
            }
        }

        return isValid;
    }

    public static boolean phoneValidator( String telefone ) {
        return telefone.matches( "((10)|([1-9][1-9])) [2-9][0-9]{3}-[0-9]{4}" );
    }

    public static boolean cpfValidator( String cpf ) {
        return cpf.matches( " (^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)" );
    }

    public static boolean tipoContatoValidator( Contato contato ) {
        switch ( contato.getTipoContato().getNome().toUpperCase() ) {
            case "TELEFONE":
                return phoneValidator( contato.getValor() );
            case "EMAIL":
                return emailValidator( contato.getValor() );
        }

    return true;
    }
}
