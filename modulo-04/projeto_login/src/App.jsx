import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { PrivateRoute } from "../src/PrivateRoute/PrivateRoute"
import PaginaInicial from "../src/Components/PaginaInicial"
import Login from "../src/Components/PaginaLogin"
import NovoPersonagem from "../src/Components/NovoPersonagem"
import TodosPersonagens from "../src/Components/TodosPersonagens"
import './App.css';

function App() {
  return (
    <Router>
      <PrivateRoute path="/" exact component={ PaginaInicial }/>
      <PrivateRoute path="/novo" exact component={ NovoPersonagem }/>
      <PrivateRoute path="/todos" exact component={ TodosPersonagens }/>
      <Route path="/login" component={ Login } />
    </Router>
  );
}

export default App;
