import React, {Component} from 'react'
import "../css/NovoPersonagem.css"

export default class NovoPersonagem extends Component {
    
    componentWillMount() {
        let header = { headers: { 
            Authorization : localStorage.getItem('Authorization') 
        }}
    }

    logout() {
        localStorage.removeItem('Authorization')
        this.props.history.push("/login")
    }

    render() {
        return(
            <React.Fragment className="cardNovoPersonagem">
                <header className="header">
                    <div>
                        <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                    </div>
                    <h5 className="titulo">Novo Personagem</h5>
                </header>
                <body>
                    <form>
                        <label className="id">ID:</label>
                        <input className="inputId"></input>
                        
                        <label className="vida">VIDA:</label>
                        <input className="inputVida"></input>

                        <label className="exp">EXPERIENCIA:</label>
                        <input className="inputExp"></input>

                        <label className="dano">DANO:</label>
                        <input className="inputDano"></input>

                        <button className="adicionarNovo" >ADICIONAR</button>
                    </form>
                </body>
            </React.Fragment>
        )
    }
} 