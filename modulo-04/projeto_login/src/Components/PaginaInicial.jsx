import React, { Component } from 'react'
import '../css/PaginaIncial.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Axios from 'axios';
export default class PaginaInicial extends Component {

    componentWillMount() {
        const { match: { params } } = this.props;
        let header = { headers: { Authorization : localStorage.getItem('Authorization') } }

        Axios.post('http://localhost:8080/api/elfo/novo', { nome: "Legloas"} ,header)

        Axios.get( 'http://localhost:8080/api/elfo/', header)
        .then(
        resp => {
            let elfos = resp.data;
            console.log('Elfos',elfos)
            this.setState({
                Listaelfos: elfos
            })
        })
    }

    logout() {
        localStorage.removeItem('Authorization')
        this.props.history.push("/login")
    }

    render() {
        return (
            <React.Fragment>
                <header className="headerPagina">
                    <Link to="/"><img className="logo"/></Link>
                    <h5 className="titulo">Página Inicial</h5>
                    <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                </header>
                <body>
                    <div className="itensInicial">
                        <Link className="btnAdicionar" to="/novo" >Adicionar Novo Personagem</Link>
                        <Link className="btnTodos" to="/todos" >Visualizar todos os Personagem</Link>
                    </div>
                </body>
            </React.Fragment>
        )
    }
}
