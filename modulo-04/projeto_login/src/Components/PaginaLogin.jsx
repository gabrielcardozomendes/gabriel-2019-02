import React, { Component } from 'react'
import * as axios from 'axios'
import '../css/Login.css'

export default class Login extends Component {
    constructor( props ) {
        super( props )
        
        this.state = {
            username: '',
            password: ''
        }
        this.trocaState = this.trocaState.bind( this )
    }

    trocaState ( evt ) {
        const { name, value } = evt.target

        this.setState ( {
            [name]: value
        } ) 
    }

    logar ( evt ) {
        evt.preventDefault()

        const { username, password } = this.state
        if( username && password ) {
            axios.post( 'http://localhost:8080/login', {
                username: this.state.username,
                password: this.state.password
            } ).then( (resp) => {
                console.log( " resp")
                localStorage.setItem( 'Authorization', resp.headers.authorization )
                this.props.history.push( "/" )
            } )
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="cardLogar">
                        <label className="logo">LOGIN</label>
                    <div className="inputs">
                        <label className="labelUsername">E-Mail</label>
                        <input type="text" className="username" name="username" id="username" placeholder="Username" onChange={ this.trocaState }/>
                        <label className="labelPassword">Password</label>
                        <input type="password" className="password" name="password" id="password" placeholder="Password" onChange={ this.trocaState }/>
                        <button type="button" className="btnLogar" onClick={ this.logar.bind( this ) }>Logar</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}