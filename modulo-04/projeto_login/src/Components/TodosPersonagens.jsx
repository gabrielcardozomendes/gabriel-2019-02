import React, {Component} from 'react'
import Axios from 'axios'
import "../css/NovoPersonagem.css"
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
export default class NovoPersonagem extends Component {
    
    constructor( props ) {
        super(props)

        this.state = {
            ListaElfos: []
        }
    } 

    componentWillMount() {
        let header = { headers: { 
            Authorization : localStorage.getItem('Authorization') 
        }}

        Axios.get( 'http://localhost:8080/api/elfo/', header ).then(
            resp => {
                let elfos = resp.data;
                this.setState({
                    ListaElfos: elfos
            })
        })
    }

    logout() {
        localStorage.removeItem('Authorization')
        this.props.history.push("/login")
    }

    render() {
        const {ListaElfos} = this.state;
        return (
        <React.Fragment>
            { ListaElfos.map(( dados ) => { 
                return(
                    <React.Fragment>
                        <header className="headerPagina">
                            <Link to="/"><img className="logo"/></Link>
                            <h5 className="titulo">Página Inicial</h5>
                            <button className="btnLogout" type="button" onClick={ () => this.logout() }>Logout</button>
                        </header>
                        <body>
                            <div className="itensInicial">
                                <h3 className="id" >ID: { dados.id }</h3>
                                <h3 className="vida" >Vida: { dados.vida }</h3>
                                <h3 className="exp" >Exp: { dados.experiencia }</h3>
                                <h3 className="raca" >Raça: { dados.raca }</h3>
                                <h3 className="dano" >Dano: { dados.dano }</h3>
                            </div>
                        </body>
                </React.Fragment>
                )
            }) }
        </React.Fragment>
        )
    }
} 