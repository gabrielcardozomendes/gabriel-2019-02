package br.com.dbccompany.vemSerSpring;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@org.springframework.context.annotation.Configuration
public class Configuration  implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry resRegistry ) {
        resRegistry.addMapping("/**").exposedHeaders("Authorization").allowedMethods("*");
    }
}
