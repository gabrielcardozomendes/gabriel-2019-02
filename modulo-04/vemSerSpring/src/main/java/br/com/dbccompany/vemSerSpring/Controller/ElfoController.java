package br.com.dbccompany.vemSerSpring.Controller;

import br.com.dbccompany.vemSerSpring.Entity.Elfo;
import br.com.dbccompany.vemSerSpring.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/elfo" )
public class ElfoController {

    @Autowired
    ElfoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Elfo> todosElfos() {
        return service.todosElfos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Elfo novoElfo( @RequestBody Elfo elfo ) {
        return service.salvar( elfo );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Elfo editarElfo( @PathVariable Integer id ,@RequestBody Elfo elfo ) {
        return service.editar( id, elfo );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public void removerElfo( @PathVariable Integer id ) {
        service.removeElfoId( id );
    }
}
