package br.com.dbccompany.vemSerSpring.Controller;

import br.com.dbccompany.vemSerSpring.Entity.Inventario;
import br.com.dbccompany.vemSerSpring.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "api/inventario" )
public class InventarioController {

    @Autowired
    InventarioService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Inventario> todosInventarios() {
        return service.todosInventarios();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Inventario novoInventario( @RequestBody Inventario inventario ) {
        return service.salvar( inventario );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Inventario editarInventario( @PathVariable Integer id, @RequestBody Inventario inventario ) {
        return service.editar( id, inventario );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public void removerInventario( @PathVariable Integer id ) {
        service.removeInventarioId( id );
    }
}
