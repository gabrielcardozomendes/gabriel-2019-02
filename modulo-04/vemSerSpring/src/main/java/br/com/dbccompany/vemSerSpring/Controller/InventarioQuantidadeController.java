package br.com.dbccompany.vemSerSpring.Controller;

import br.com.dbccompany.vemSerSpring.Entity.InventarioQuantidade;
import br.com.dbccompany.vemSerSpring.Service.InventarioQuantidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "api/inventarioQuantidade" )
public class InventarioQuantidadeController {
    @Autowired
    InventarioQuantidadeService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<InventarioQuantidade> todos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public InventarioQuantidade novo( @RequestBody InventarioQuantidade inventarioQuantidade ) {
        return service.salvar( inventarioQuantidade );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public InventarioQuantidade editar( @PathVariable Integer id ,@RequestBody InventarioQuantidade inventarioQuantidade ) {
        return service.editar( id, inventarioQuantidade );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public void remover( @PathVariable Integer id ) {
        service.removeId( id );
    }
}
