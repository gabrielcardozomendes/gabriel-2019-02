package br.com.dbccompany.vemSerSpring.Controller;

import br.com.dbccompany.vemSerSpring.Entity.Item;
import br.com.dbccompany.vemSerSpring.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/item" )
public class ItemController {

    @Autowired
    ItemService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Item> todosItens() {
        return service.todosItens();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Item novoItem( @RequestBody Item item ) {
        return service.salvar( item );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Item editarItem( @PathVariable Integer id, @RequestBody Item item ) {
        return service.editar( id, item );
    }

    @DeleteMapping( value = "/deletar/{id}" )
    @ResponseBody
    public void removerItem( @PathVariable Integer id ) {
        service.removeItemId( id );
    }
}
