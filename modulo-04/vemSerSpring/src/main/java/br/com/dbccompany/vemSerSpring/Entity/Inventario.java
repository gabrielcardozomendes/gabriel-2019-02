package br.com.dbccompany.vemSerSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
public class Inventario {

    @Id
    @GeneratedValue( generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;
    private Integer tamanho;

    @OneToMany( mappedBy = "inventario" )
    private List<InventarioQuantidade> inventarioQuantidade = new ArrayList<>();

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    @OneToOne
    @JoinColumn( name = "fk_id_personagem" )
    private Personagem personagem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<InventarioQuantidade> getInventarioQuantidade() {
        return inventarioQuantidade;
    }

    public void setInventarioQuantidade(List<InventarioQuantidade> inventarioQuantidade) {
        this.inventarioQuantidade = inventarioQuantidade;
    }

    public Personagem getPersonagem() {
        return personagem;
    }

    public void setPersonagem(Personagem personagem) {
        this.personagem = personagem;
    }
}
