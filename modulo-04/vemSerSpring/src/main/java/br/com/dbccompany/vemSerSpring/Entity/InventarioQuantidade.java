package br.com.dbccompany.vemSerSpring.Entity;

import javax.persistence.*;

@Entity
@SequenceGenerator( allocationSize = 1, name = "QUANTIDADE_SEQ", sequenceName = "QUANTIDADE_SEQ")
public class InventarioQuantidade {

    @Id
    @GeneratedValue( generator = "QUANTIDADE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "fk_id_inventario" )
    private Inventario inventario;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "fk_id_item" )
    private Item item;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Inventario getIntenvario() {
        return inventario;
    }

    public void setIntenvario(Inventario intenvario) {
        this.inventario = intenvario;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
