package br.com.dbccompany.vemSerSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
public class Item {

    @Id
    @GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "DESCRICAO", nullable = false )
    protected String descricao;

    @OneToMany( mappedBy = "item" )
    private List<InventarioQuantidade> inventarioQuantidade = new ArrayList<InventarioQuantidade>();

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<InventarioQuantidade> getInventarioQuantidade() {
        return inventarioQuantidade;
    }

    public void setInventarioQuantidade(List<InventarioQuantidade> inventarioQuantidade) {
        this.inventarioQuantidade = inventarioQuantidade;
    }
}
