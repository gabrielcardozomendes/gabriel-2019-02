package br.com.dbccompany.vemSerSpring.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "PERSONAGENS_SEQ", sequenceName = "PERSONAGENS_SEQ")
public abstract class Personagem {

    @Id
    @GeneratedValue( generator = "PERSONAGENS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "EXPERIENCIA", nullable = false)
    private Integer experiencia;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "VIDA", nullable = false )
    private Double vida;

    @Column( name = "DANO", nullable = false )
    private Double dano;

    @Enumerated( EnumType.STRING )
    @Column( name = "STATUS" )
    private Status status;

    @Enumerated( EnumType.STRING )
    @Column( name = "RACA" )
    private RacaType raca;

    @OneToOne( mappedBy = "personagem" )
    private Inventario inventario;

    {
        status = Status.RECEM_CRIADO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getDano() {
        return dano;
    }

    public void setDano(Double dano) {
        this.dano = dano;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public RacaType getRaca() {
        return raca;
    }

    protected void setRaca(RacaType raca) {
        this.raca = raca;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }
}
