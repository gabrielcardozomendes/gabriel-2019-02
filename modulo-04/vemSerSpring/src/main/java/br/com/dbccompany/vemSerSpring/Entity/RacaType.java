package br.com.dbccompany.vemSerSpring.Entity;

public enum RacaType {
    ELFO,
    DWARF,
    DWARF_BARBA_LONGA,
    ELFO_VERDE,
    ELFO_NOTURNO,
    ELFO_DA_LUZ
}
