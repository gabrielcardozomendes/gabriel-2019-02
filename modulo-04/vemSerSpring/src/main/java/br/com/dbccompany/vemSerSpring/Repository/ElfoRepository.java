package br.com.dbccompany.vemSerSpring.Repository;

import br.com.dbccompany.vemSerSpring.Entity.Elfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ElfoRepository extends CrudRepository< Elfo, Integer > {

    //Elfo findByDano( Double dano );
    List<Elfo> findAll();
}
