package br.com.dbccompany.vemSerSpring.Repository;

import br.com.dbccompany.vemSerSpring.Entity.InventarioQuantidade;
import org.springframework.data.repository.CrudRepository;

public interface InventarioQuantidadeRepository extends CrudRepository<InventarioQuantidade, Integer> {

}
