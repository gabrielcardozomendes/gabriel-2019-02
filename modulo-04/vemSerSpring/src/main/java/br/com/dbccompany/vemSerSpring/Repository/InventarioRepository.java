package br.com.dbccompany.vemSerSpring.Repository;

import br.com.dbccompany.vemSerSpring.Entity.Inventario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventarioRepository extends CrudRepository< Inventario, Integer > {
    List<Inventario> findAll();
}
