package br.com.dbccompany.vemSerSpring.Repository;

import br.com.dbccompany.vemSerSpring.Entity.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository< Item, Integer >{
    List<Item> findAll();
}
