package br.com.dbccompany.vemSerSpring.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Config do http
        http.headers().frameOptions().sameOrigin().and()
                .csrf().disable().authorizeRequests()
                .antMatchers( "/home" ).permitAll()
                .antMatchers( HttpMethod.POST, "/login" ).permitAll()
                .anyRequest().authenticated().and()
                .cors()
                .and()

                //filtra requisicao de login
                .addFilterBefore( new JWTLoginFilter( "/login", authenticationManager() ),
                        UsernamePasswordAuthenticationFilter.class )

                //filtra outras requisicoes e verifica JWT no Header
                .addFilterBefore( new JWTAuthenticationFilter(),
                        UsernamePasswordAuthenticationFilter.class );
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth ) throws Exception {
        //Config do Login
        auth.inMemoryAuthentication()
                .withUser( "usuario" )
                .password( "{noop}123456" )
                .roles( "ADMIN" );
    }
}
