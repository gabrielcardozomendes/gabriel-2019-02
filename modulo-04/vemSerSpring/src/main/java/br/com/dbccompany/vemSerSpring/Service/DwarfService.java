package br.com.dbccompany.vemSerSpring.Service;

import br.com.dbccompany.vemSerSpring.Entity.Dwarf;
import br.com.dbccompany.vemSerSpring.Repository.DwarfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DwarfService {

    @Autowired
    private DwarfRepository dwarfRepository;

    @Transactional( rollbackFor = Exception.class )
    public Dwarf salvar(Dwarf dwarf ) {
        return dwarfRepository.save( dwarf );
    }

    @Transactional( rollbackFor = Exception.class )
    public Dwarf editar( Integer id, Dwarf dwarf ) {
        dwarf.setId( id );
        return dwarfRepository.save( dwarf );
    }

    public List<Dwarf> todosDwarfs() {
        return ( List<Dwarf> ) dwarfRepository.findAll();
    }

    public void removeDwarfId( Integer id ) {
        dwarfRepository.deleteById( id );
    }
}
