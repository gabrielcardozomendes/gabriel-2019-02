package br.com.dbccompany.vemSerSpring.Service;

import br.com.dbccompany.vemSerSpring.Entity.Inventario;
import br.com.dbccompany.vemSerSpring.Entity.InventarioQuantidade;
import br.com.dbccompany.vemSerSpring.Entity.Item;
import br.com.dbccompany.vemSerSpring.Repository.InventarioQuantidadeRepository;
import br.com.dbccompany.vemSerSpring.Repository.InventarioRepository;
import br.com.dbccompany.vemSerSpring.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InventarioQuantidadeService {

    @Autowired
    private InventarioQuantidadeRepository repository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private InventarioRepository inventarioRepository;

    @Transactional( rollbackFor = Exception.class )
    public InventarioQuantidade salvar(InventarioQuantidade inventarioQuantidade ) {
        Item itemObject = inventarioQuantidade.getItem();
        Inventario inventarioObject = inventarioQuantidade.getIntenvario();

        if(itemObject.getId() == null) {
            itemObject = itemRepository.save(itemObject);
        }

        if(inventarioObject.getId() == null) {
            inventarioObject = inventarioRepository.save(inventarioObject);
        }


        inventarioQuantidade.setIntenvario(inventarioObject);
        inventarioQuantidade.setItem(itemObject);

        return repository.save( inventarioQuantidade );
    }

    @Transactional( rollbackFor = Exception.class )
    public InventarioQuantidade editar( Integer id, InventarioQuantidade inventarioQuantidade ) {
        inventarioQuantidade.setId( id );
        return repository.save( inventarioQuantidade );
    }

    public List<InventarioQuantidade> todos() {
        return ( List<InventarioQuantidade> ) repository.findAll();
    }

    public void removeId( Integer id ) {
        repository.deleteById( id );
    }
}
