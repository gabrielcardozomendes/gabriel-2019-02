package br.com.dbccompany.vemSerSpring.Service;

import br.com.dbccompany.vemSerSpring.Entity.Item;
import br.com.dbccompany.vemSerSpring.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Transactional( rollbackFor = Exception.class )
    public Item salvar(Item item ) {
        return itemRepository.save( item );
    }

    @Transactional( rollbackFor = Exception.class )
    public Item editar( Integer id, Item item ) {
        item.setId( id );
        return itemRepository.save( item );
    }

    public List<Item> todosItens() {
        return ( List<Item> ) itemRepository.findAll();
    }

    public void removeItemId( Integer id ) {
        itemRepository.deleteById( id );
    }
}
