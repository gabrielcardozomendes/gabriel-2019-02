package br.com.dbccompany.vemSerSpring;

import br.com.dbccompany.vemSerSpring.Entity.Dwarf;
import br.com.dbccompany.vemSerSpring.Entity.Elfo;
import br.com.dbccompany.vemSerSpring.Service.DwarfService;
import br.com.dbccompany.vemSerSpring.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VemSerSpringApplication {
	public static void main(String[] args) {
		SpringApplication.run(VemSerSpringApplication.class, args);
	}
}
