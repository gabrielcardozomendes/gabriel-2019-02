import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main( String[] args ) {
        Connection conn = Connector.connect();

        /*try {
            ResultSet rs = conn.prepareStatement( "select tname from tab where tname = 'PAISES'" ).executeQuery();

            if ( !rs.next() ) {
                conn.prepareStatement( "CREATE TABLE PAISES(\n" + "ID_PAIS INTEGER NOT NULL PRIMARY KEY,\n" + "NOME VARCHAR(100) NOT NULL \n" + ")" ).execute();
            }

            ResultSet nome = conn.prepareStatement( "select nome from paises where nome = 'Brasil'" ).executeQuery();

            if( !nome.next() ){
                PreparedStatement pst = conn.prepareStatement( "insert into paises(id_pais, nome) " + "values(paises_seq.nextval, ?)" );
                pst.setString( 1, "Brasil" );;
                pst.executeUpdate();
            }else {
                System.out.println("Pais Existente");
            }

            rs = conn.prepareStatement( "select * from paises" ).executeQuery();

            while ( rs.next() ) {
                System.out.println( String.format( "Nome do Pais: %s", rs.getString( "nome" ) ) );
            }

        } catch ( SQLException ex ) {
            Logger.getLogger( Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN", ex );
        }*/

        /*try {
         ResultSet rs = conn.prepareStatement( "select tname from tab where tname = 'ESTADOS'" ).executeQuery();

         if( !rs.next() ) {
            conn.prepareStatement( "CREATE TABLE ESTADOS(\n" + "ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n" + "NOME VARCHAR(100) NOT NULL \n" + ")").execute();
         }

         PreparedStatement pst = conn.prepareStatement( "insert into ESTADOS(ID_ESTADO, NOME)" + " values(ESTADOS_SEQ.nextval, ?)" );
         pst.setString( 1, "São Paulo");
         pst.executeUpdate();

         rs = conn.prepareStatement( "select * from ESTADOS " ).executeQuery();

         while(rs.next()) {
             System.out.println(String.format("Nome do Estado: %s", rs.getString( "NOME" )));
            }

        }catch ( SQLException ex ) {
            Logger.getLogger( Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN", ex );
        }*/

        /*try {
            ResultSet rs = conn.prepareStatement( "select tname from tab where tname = 'CIDADES'" ).executeQuery();

            if( !rs.next() ) {
                conn.prepareStatement( "CREATE TABLE CIDADES(\n" + "ID_CIDADE INTEGER NOT NULL PRIMARY KEY,\n" + "NOME VARCHAR(100) NOT NULL \n" + ")").execute();
            }

            PreparedStatement pst = conn.prepareStatement( "insert into CIDADES(ID_CIDADE, NOME)" + " values(CIDADES_SEQ.nextval, ?)" );
            pst.setString( 1, "Gravataí");
            pst.executeUpdate();

            rs = conn.prepareStatement( "select * from CIDADES " ).executeQuery();

            while(rs.next()) {
                System.out.println(String.format("Nome da cidade: %s", rs.getString( "NOME" )));
            }

        }catch ( SQLException ex ) {
            Logger.getLogger( Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN", ex );
        }*/

        /*try {
            ResultSet rs = conn.prepareStatement( "select tname from tab where tname = 'BAIRROS'" ).executeQuery();

            if( !rs.next() ) {
                conn.prepareStatement( "CREATE TABLE BAIRROS(\n" + "ID_BAIRROS INTEGER NOT NULL PRIMARY KEY,\n" + "NOME VARCHAR(100) NOT NULL \n" + ")").execute();
            }

            PreparedStatement pst = conn.prepareStatement( "insert into BAIRROS(ID_BAIRROS, NOME)" + " values(CIDADES_SEQ.nextval, ?)" );
            pst.setString( 1, "São Geraldo");
            pst.executeUpdate();

            rs = conn.prepareStatement( "select * from BAIRROS " ).executeQuery();

            while(rs.next()) {
                System.out.println(String.format("Nome do Bairro: %s", rs.getString( "NOME" )));
            }

        }catch ( SQLException ex ) {
            Logger.getLogger( Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN", ex );
        }*/
     }
}
