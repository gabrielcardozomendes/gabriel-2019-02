package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Banco;

public class BancosDAO extends AbstractDAO< Banco > {

	@Override
	protected Class< Banco > getEntityClass() {
		return Banco.class;
	}

}
