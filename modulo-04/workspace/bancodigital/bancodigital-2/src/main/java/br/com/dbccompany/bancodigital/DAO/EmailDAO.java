package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Email;

public class EmailDAO extends AbstractDAO< Email > {

	@Override
	protected Class< Email > getEntityClass() {
		return Email.class;
	}

}
