package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Telefone;

public class TelefoneDAO extends AbstractDAO< Telefone > {

	@Override
	protected Class< Telefone > getEntityClass() {
		return Telefone.class;
	}

}
