package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "BANCOS_SEQ", sequenceName = "BANCOS_SEQ" )
public class Banco extends AbstractEntity {

	public static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "BAIRROS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@Column( name = "CODIGO", nullable = false )
	private Integer codigo;
	
	@Column( name = "NOME", length = 100, nullable = false )
	private String nome;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public Integer getId() {
		return id;
	}
	
}
