package br.com.dbccompany.bancodigital.Entity;

public enum TipoCorrentista {
	PJ, PF, CONJUNTA, INVESTIMENTO;
}
