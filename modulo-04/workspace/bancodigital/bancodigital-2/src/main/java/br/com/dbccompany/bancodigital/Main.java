package br.com.dbccompany.bancodigital;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) {
		
		Session session = null;
		Transaction transaction = null;
		
		try {
			session = HibernateUtil.getSession();
			transaction = session.beginTransaction();
			
			Paises paises = new Paises();
			paises.setNome("Brasil");
			
			session.save(paises);
			
			transaction.commit();
		} catch (Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			LOG.log( Level.SEVERE, e.getMessage(), e );
			System.exit(1);
		} finally {
			if( session != null ) {
				session.close();
			}
		}
		System.exit(0);
	}

}
