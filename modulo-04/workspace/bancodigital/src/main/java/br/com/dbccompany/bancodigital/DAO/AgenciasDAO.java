package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasDAO extends AbstractDAO< Agencias > {

	public Agencias parseFrom( AgenciasDTO dto ) {
		Agencias agencias = null;
		
		if( dto.getIdAgencia() != null ) {
			agencias = buscar( dto.getIdAgencia() );
		} else {
			agencias = new Agencias();
		}
		
		agencias.setNome( dto.getNome() );
		
		return agencias;
	}
	
	public Agencias parseFrom( Agencias agencia ) {
		Agencias agencias = null;
		
		if( agencia.getId() != null ) {
			agencias = buscar( agencia.getId() );
		} else {
			agencias = new Agencias();
		}
		
		agencias.setNome( agencia.getNome() );
		
		return agencias;
	}
	
	@Override
	protected Class< Agencias > getEntityClass() {
		return Agencias.class;
	}

}
