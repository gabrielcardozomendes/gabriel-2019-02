package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosDAO extends AbstractDAO< Bairros >{

	public Bairros parseFrom( BairrosDTO dto ) {
		Bairros bairros = null;
		
		if( dto.getIdBairro() != null  ) {
			bairros = buscar( dto.getIdBairro() );
		} else {
			bairros = new Bairros();
		}
		
		bairros.setNome( dto.getNome() );
		
		return bairros;
	}
	
	public Bairros parseFrom( Bairros bairro ) {
		Bairros bairros = null;
		
		if( bairro.getId() != null  ) {
			bairros = buscar( bairro.getId() );
		} else {
			bairros = new Bairros();
		}
		
		bairros.setNome( bairro.getNome() );
		
		return bairros;
	}
	
	
	@Override
	protected Class< Bairros > getEntityClass() {
		return Bairros.class;
	}

}
