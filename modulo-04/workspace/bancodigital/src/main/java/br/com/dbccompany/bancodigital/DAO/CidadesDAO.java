package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class CidadesDAO extends AbstractDAO< Cidades > {

	
	public Cidades parseFrom( CidadesDTO dto ) {
		Cidades cidades = null;
		
		if( dto.getIdCidade() != null ) {
			cidades = buscar( dto.getIdCidade() );
		} else {
			cidades = new Cidades();
		}
		
		cidades.setNome( dto.getNome() );
		
		return cidades;
	}
	
	public Cidades parseFrom( Cidades cidade ) {
		Cidades cidades = null;
		
		if( cidade.getId() != null ) {
			cidades = buscar( cidade.getId() );
		} else {
			cidades = new Cidades();
		}
		
		return cidades;
	}
	@Override
	protected Class< Cidades > getEntityClass() {
		return Cidades.class;
	}

}
