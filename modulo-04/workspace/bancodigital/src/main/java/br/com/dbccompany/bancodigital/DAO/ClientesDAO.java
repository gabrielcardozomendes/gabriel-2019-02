package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesDAO extends AbstractDAO< Clientes >{

	public Clientes parseFrom( ClientesDTO dto ) {
		Clientes clientes = null;
		
		if( dto.getIdCliente() != null ) {
			clientes = buscar( dto.getIdCliente() );
		} else {
			clientes = new Clientes();
		}
		
		clientes.setNome( dto.getNome() );
		
		return clientes;
	}
	
	public Clientes parseFrom( Clientes cliente ) {
		Clientes clientes = null;
		
		if( cliente.getId() != null ) {
			clientes = buscar( cliente.getId() );
		} else {
			clientes = new Clientes();
		}
		
		return clientes;
	}
	
	@Override
	protected Class< Clientes > getEntityClass() {
		return Clientes.class;
	}

}
