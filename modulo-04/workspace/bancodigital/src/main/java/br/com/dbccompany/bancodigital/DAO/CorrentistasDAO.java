package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.CorrentistaDTO;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class CorrentistasDAO extends AbstractDAO< Correntistas > {

	public Correntistas parseFrom( CorrentistaDTO dto ) {
		Correntistas correntista = null;
		
		if( dto.getIdCorrentista() != null ) {
			correntista = buscar( dto.getIdCorrentista() );
		} else {
			correntista = new Correntistas();
		}
		
		correntista.setCnpj( dto.getCnpj() );
		
		return correntista;
	}
	
	public Correntistas parseFrom( Correntistas correntistas ) {
		Correntistas correntista = null;
		
		if( correntistas.getId() != null ) {
			correntista = buscar( correntistas.getId() );
		} else {
			correntista = new Correntistas();
		}
		
		return correntista;
	}
	
	@Override
	protected Class< Correntistas > getEntityClass() {
		return Correntistas.class;
	}

}
