package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.EmailDTO;
import br.com.dbccompany.bancodigital.Entity.Email;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class EmailDAO extends AbstractDAO< Email > {

	public Email parseFrom( EmailDTO dto ) {
		Email email = null;
		
		if( dto.getIdEmail() != null ) {
			email = buscar( dto.getIdEmail()  );
		} else {
			email = new Email();
		}
		
		email.setValor( dto.getValor() );
		
		return email;
	}
	
	public Email parseFrom( Email emails ) {
		Email email = null;
		
		if( emails.getId() != null ) {
			email = buscar( emails.getId()  );
		} else {
			email = new Email();
		}
		
		return email;
	}
	
	@Override
	protected Class< Email > getEntityClass() {
		return Email.class;
	}

}
