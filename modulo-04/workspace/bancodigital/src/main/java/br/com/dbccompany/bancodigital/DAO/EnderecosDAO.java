package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO< Enderecos > {

	public Enderecos parseFrom( EnderecosDTO dto ) {
		Enderecos enderecos = null;
		
		if( dto.getIdEnderecos() != null ) {
			enderecos = buscar( dto.getIdEnderecos() );
		} else {
			enderecos = new Enderecos();
		}
		
		enderecos.setLogradouro( dto.getLogradouro() );
		
		return enderecos;
	}
	
	public Enderecos parseFrom( Enderecos endereco ) {
		Enderecos enderecos = null;
		
		if( endereco.getId() != null ) {
			enderecos = buscar( endereco.getId() );
		} else {
			enderecos = new Enderecos();
		}
		
		return enderecos;
	}
	
	@Override
	protected Class< Enderecos > getEntityClass() {
		return Enderecos.class;
	}

}
