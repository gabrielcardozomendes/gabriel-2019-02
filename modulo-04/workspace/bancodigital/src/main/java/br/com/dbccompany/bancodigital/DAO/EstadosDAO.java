package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosDAO extends AbstractDAO< Estados > {

	public Estados parseFrom( EstadosDTO dto ) {
		Estados estados = null;
		
		if( dto.getIdEstados() != null ) {
			estados = buscar( dto.getIdEstados() );
		} else {
			estados = new Estados();
		}
		
		estados.setNome( dto.getNome() );
		
		return estados;
	}
	
	public Estados parseFrom( Estados estado ) {
		Estados estados = null;
		
		if( estado.getId() != null ) {
			estados = buscar( estado.getId() );
		} else {
			estados = new Estados();
		}
		
		return estados;
	}
	
	@Override
	protected Class< Estados > getEntityClass() {
		return Estados.class;
	}

}
