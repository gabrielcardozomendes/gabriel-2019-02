package br.com.dbccompany.bancodigital.DAO;

import br.com.dbccompany.bancodigital.Dto.TelefoneDTO;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesDAO extends AbstractDAO<Telefones> {
	
	public Telefones parseFrom( TelefoneDTO dto ) {
		Telefones telefone = null;
		
		if( dto.getIdTelefone() != null ) {
			telefone = buscar( dto.getIdTelefone() );
		} else {
			telefone = new Telefones();
		}
		
		telefone.setNumero( dto.getNumero() );
		
		return telefone;
	}
	
	public Telefones parseFrom( Telefones telefones ) {
		Telefones telefone = null;
		
		if( telefones.getId() != null ) {
			telefone = buscar( telefones.getId() );
		} else {
			telefone = new Telefones();
		}
		
		return telefone;
	}
	
	@Override
	protected Class<Telefones> getEntityClass() {
		return Telefones.class;
	}
}
