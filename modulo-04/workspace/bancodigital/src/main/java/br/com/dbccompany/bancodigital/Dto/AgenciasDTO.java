package br.com.dbccompany.bancodigital.Dto;

public class AgenciasDTO {

	private Integer idAgencia;
	private Integer codigo;
	
	private String nome;
	
	private EnderecosDTO endereco;
	
	private BancosDTO banco;

	public Integer getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(Integer idAgencia) {
		this.idAgencia = idAgencia;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EnderecosDTO getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecosDTO endereco) {
		this.endereco = endereco;
	}

	public BancosDTO getBanco() {
		return banco;
	}

	public void setBanco(BancosDTO banco) {
		this.banco = banco;
	}
}
