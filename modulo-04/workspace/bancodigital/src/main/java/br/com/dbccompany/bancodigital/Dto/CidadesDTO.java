package br.com.dbccompany.bancodigital.Dto;

public class CidadesDTO {

	private Integer idCidade;
	
	private String nome;
	
	private EstadosDTO estado;

	public Integer getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(Integer idCidade) {
		this.idCidade = idCidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadosDTO getEstado() {
		return estado;
	}

	public void setEstado(EstadosDTO estado) {
		this.estado = estado;
	}
}
