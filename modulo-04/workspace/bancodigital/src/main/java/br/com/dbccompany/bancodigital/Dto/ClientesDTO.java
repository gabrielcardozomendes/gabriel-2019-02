package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.EstadoCivil;

public class ClientesDTO {

	private Integer idCliente;
	
	private String cpf;
	private String nome;
	private String rg;
	private String conjugue;
	private String dataNascimento;
	private EstadoCivil estadoCivil;
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getConjugue() {
		return conjugue;
	}
	public void setConjugue(String conjugue) {
		this.conjugue = conjugue;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
}
