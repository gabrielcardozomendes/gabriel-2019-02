package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.TipoCorrentista;

public class CorrentistaDTO {

	private Integer idCorrentista;
	private String razaoSocial;
	private String cnpj;
	
	private TipoCorrentista tipoCorrentista;

	public Integer getIdCorrentista() {
		return idCorrentista;
	}

	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public TipoCorrentista getTipoCorrentista() {
		return tipoCorrentista;
	}

	public void setTipoCorrentista(TipoCorrentista tipoCorrentista) {
		this.tipoCorrentista = tipoCorrentista;
	}
}
