package br.com.dbccompany.bancodigital.Dto;

public class EmailDTO {
	
	private Integer idEmail;
	
	private String valor;

	public Integer getIdEmail() {
		return idEmail;
	}

	public void setIdEmail(Integer idEmail) {
		this.idEmail = idEmail;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}
