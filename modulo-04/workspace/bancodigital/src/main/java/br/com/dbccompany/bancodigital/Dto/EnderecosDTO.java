package br.com.dbccompany.bancodigital.Dto;

public class EnderecosDTO {

	private Integer idEnderecos;
	private Integer numero;
	
	private String logradouro;
	private String complemento;
	
	private BairrosDTO bairro;
	
	private AgenciasDTO agencia;

	public Integer getIdEnderecos() {
		return idEnderecos;
	}

	public void setIdEnderecos(Integer idEnderecos) {
		this.idEnderecos = idEnderecos;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public BairrosDTO getBairro() {
		return bairro;
	}

	public void setBairro(BairrosDTO bairro) {
		this.bairro = bairro;
	}

	public AgenciasDTO getAgencia() {
		return agencia;
	}

	public void setAgencia(AgenciasDTO agencia) {
		this.agencia = agencia;
	}
}
