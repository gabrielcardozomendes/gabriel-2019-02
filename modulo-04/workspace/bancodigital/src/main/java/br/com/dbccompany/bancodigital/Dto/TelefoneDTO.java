package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.TipoTelefone;

public class TelefoneDTO {
 
	
	private Integer idTelefone;
	
	private TipoTelefone tipoTelefone;
	
	private String numero;

	public Integer getIdTelefone() {
		return idTelefone;
	}

	public void setIdTelefone(Integer idTelefone) {
		this.idTelefone = idTelefone;
	}

	public TipoTelefone getTipoTelefone() {
		return tipoTelefone;
	}

	public void setTipoTelefone(TipoTelefone tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}
