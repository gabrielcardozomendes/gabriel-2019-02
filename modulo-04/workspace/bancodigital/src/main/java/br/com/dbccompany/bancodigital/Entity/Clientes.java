package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )
public class Clientes extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@Enumerated( EnumType.STRING )
	private EstadoCivil estadoCivil;
	
	@Column( name = "CPF", length = 11, nullable = false )
	private String cpf;
	
	@Column( name = "NOME", length = 100, nullable = false )
	private String nome;
	
	@Column( name = "RG", length = 11, nullable = false)
	private String rg;
	
	@Column( name = "CONJUGE", length = 100, nullable = false )
	private String conjuge;
	
	@Column( name = "DATA_NASCIMENTO", length = 8, nullable = false )
	private String dataNascimento;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_endereco" )
	private Enderecos endereco;
	
	@ManyToMany( mappedBy = "clientes" )
	private List<Correntistas> correntistas = new ArrayList<>();
	
	public List<Correntistas> getCorrentistas() {
		return correntistas;
	}

	public void setCorrentistas(List<Correntistas> correntistas) {
		this.correntistas = correntistas;
	}

	public List<Email> getEmail() {
		return email;
	}

	public void setEmail(List<Email> email) {
		this.email = email;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	@OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
	private List<Email> email = new ArrayList<>();
	
	@ManyToMany( mappedBy = "clientes" )
	private List<Telefone> telefones = new ArrayList<>();

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Enderecos getEndereco() {
		return endereco;
	}

	public void setEndereco(Enderecos endereco) {
		this.endereco = endereco;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}
	
	
}
