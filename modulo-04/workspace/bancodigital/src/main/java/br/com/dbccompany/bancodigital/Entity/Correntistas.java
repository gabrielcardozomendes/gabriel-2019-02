package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ" )
public class Correntistas extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@Column( name = "RAZAO_SOCIAL", length = 100 )
	private String razaoSocial;
	
	@Column( name = "CNPJ" )
	private String cnpj;
	
	@Column( name = "TIPO", nullable = false )
	@Enumerated( EnumType.STRING )
	private TipoCorrentista tipo;
	
	@Column( name = "SALDO" )
	private Double saldo;
	
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public List<Clientes> getClientes() {
		return clientes;
	}
	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}
	public List<Agencias> getAgencias() {
		return agencias;
	}
	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "correntistas_x_clientes", joinColumns = {
			@JoinColumn( name = "fk_id_correntistas" ) },
			inverseJoinColumns = {
					@JoinColumn( name = "fk_id_clientes" )
			})
	
	private List<Clientes> clientes = new ArrayList<>();
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "correntistas_x_agencias", joinColumns = {
			@JoinColumn( name = "fk_id_correntistas" ) },
			inverseJoinColumns = {
					@JoinColumn( name = "fk_id_agencias" )
			})
	private List<Agencias> agencias = new ArrayList<>();
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public TipoCorrentista getTipo() {
		return tipo;
	}
	public void setTipo(TipoCorrentista tipo) {
		this.tipo = tipo;
	}
	@Override
	public Integer getId() {
		return id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}
	
}
