package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ENDERECOS_SEQ", sequenceName = "ENDERCOS_SEQ" )
public class Enderecos extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "ENDERECOS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@Column( name = "NUMERO", nullable = false )
	private Integer numero;
	
	@Column( name = "LOGRADOURO", length = 100, nullable = false )
	private String logradouro;
	
	@Column( name = "COMPLEMENTO", length = 100, nullable = false )
	private String complemento;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_bairros" )
	private Bairros bairros;
	
	@OneToMany( mappedBy = "endereco" )
	private List<Clientes> clientes = new ArrayList<>();

	@OneToOne( mappedBy = "endereco" )
	private Agencias agencia;
	
	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairros getBairros() {
		return bairros;
	}

	public void setBairros(Bairros bairros) {
		this.bairros = bairros;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}
	
}
