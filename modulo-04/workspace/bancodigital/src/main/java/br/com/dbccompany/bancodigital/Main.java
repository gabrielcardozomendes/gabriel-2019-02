package br.com.dbccompany.bancodigital;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.EstadoCivil;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.Entity.TipoCorrentista;
import br.com.dbccompany.bancodigital.Services.AgenciasService;
import br.com.dbccompany.bancodigital.Services.BairrosService;
import br.com.dbccompany.bancodigital.Services.BancosService;
import br.com.dbccompany.bancodigital.Services.CidadesService;
import br.com.dbccompany.bancodigital.Services.ClientesService;
import br.com.dbccompany.bancodigital.Services.CorrentistasService;
import br.com.dbccompany.bancodigital.Services.EnderecosService;
import br.com.dbccompany.bancodigital.Services.EstadosService;
import br.com.dbccompany.bancodigital.Services.PaisesService;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) {
		
		//Services
		PaisesService service = new PaisesService();
		BancosService bancoService = new BancosService();
		EstadosService estadoService = new EstadosService();
		CidadesService cidadeService = new CidadesService();
		BairrosService bairrosService = new BairrosService();
		EnderecosService enderecosService = new EnderecosService();
		AgenciasService agenciasService = new AgenciasService();
		ClientesService clientesService = new ClientesService();
		CorrentistasService correntistasService = new CorrentistasService();
		
		//Entidades
		Paises paises = new Paises();
		
		Bancos banco = new Bancos();
		Bancos banco2 = new Bancos();
		
		Estados estado = new Estados();
		
		Cidades cidade = new Cidades();
		
		Bairros bairro = new Bairros();
		Bairros bairro2 = new Bairros();
		
		Enderecos endereco = new Enderecos();
		Enderecos endereco2 = new Enderecos();
		Enderecos endereco3 = new Enderecos();
		Enderecos endereco4 = new Enderecos();
		
		Agencias agencia = new Agencias();
		Agencias agencia2 = new Agencias();
		
		Clientes cliente = new Clientes();
		Clientes cliente2 = new Clientes();
		Clientes cliente3 = new Clientes();
		Clientes cliente4 = new Clientes();
		
		Correntistas correntista = new Correntistas();
		Correntistas correntista2 = new Correntistas();
		Correntistas correntista3 = new Correntistas();
		Correntistas correntista4 = new Correntistas();
		
		paises.setNome(" Brasil ");
		
		service.salvarPaises( paises );
		//
		banco.setNome( "Alfa" );
		banco.setCodigo( 110 );
		
		bancoService.salvarBancos( banco );
		//
		banco2.setNome( "Omega" );
		banco2.setCodigo( 1010 );
		
		bancoService.salvarBancos( banco2 );
		//
		estado.setNome( "Rio Grande do Sul" );
		estado.setPais( paises );
		
		estadoService.salvarEstados( estado );
		//
		cidade.setNome( "Gravata�" );
		cidade.setEstado( estado );
		
		cidadeService.salvarCidades( cidade );
		//
		bairro.setNome( "S�o Geraldo" );
		bairro.setCidade( cidade );
		
		bairrosService.salvarBairros( bairro );
		//
		bairro2.setNome( "Barnab�" );
		bairro2.setCidade( cidade );
		
		bairrosService.salvarBairros( bairro2 );
		//
		endereco.setBairros( bairro );
		endereco.setComplemento( "Casa" );
		endereco.setLogradouro( "Av.Brasil" );
		endereco.setNumero( 929 );
		
		enderecosService.salvarEnderecos( endereco );
		//
		endereco2.setBairros( bairro );
		endereco2.setComplemento( "Ap" );
		endereco2.setLogradouro( "Av.Andara�" );
		endereco2.setNumero( 999 );
		
		enderecosService.salvarEnderecos( endereco2 );
		//
		agencia.setCodigo( 3301 );
		agencia.setEndereco( endereco );
		agencia.setNome( "Aquela l�" );
		
		List<Correntistas> correntistas = new ArrayList<Correntistas>();
		correntistas.add( correntista );
		agencia.setCorrentistas( correntistas );
		
		agenciasService.salvarAgencias( agencia );
		
		agencia2.setCodigo( 1033 );
		agencia2.setEndereco(endereco3);
		agencia2.setNome( "Uma outra ai" );
		
		List<Correntistas> correntistas2 = new ArrayList<Correntistas>();
		correntistas2.add( correntista2 );
		agencia2.setCorrentistas( correntistas2 );
		
		agenciasService.salvarAgencias( agencia2 );
		
		cliente.setNome( "Rexxar" );
		cliente.setCpf( "001.231-30" );
		cliente.setRg( "123123" );
		cliente.setEndereco( endereco );
		cliente.setDataNascimento( "29/1/101" );
		cliente.setConjuge( "Qualquer" );
		cliente.setEstadoCivil( EstadoCivil.SOLTEIRO );
		
		List<Correntistas> listaCorrentistas = new ArrayList<Correntistas>();
		listaCorrentistas.add( correntista );
		cliente.setCorrentistas( listaCorrentistas );
		
		clientesService.salvarClientes( cliente );
		//
		cliente2.setNome( "Garrosh" );
		cliente2.setCpf( "010.334-03" );
		cliente2.setRg( "321321" );
		cliente2.setEndereco( endereco2 );
		cliente2.setDataNascimento( "30/1/100" );
		cliente2.setConjuge( "Qualquer" );
		cliente2.setEstadoCivil( EstadoCivil.DIVORCIADO );
		
		clientesService.salvarClientes( cliente2 );
		//
		correntista.setTipo( TipoCorrentista.PF );
		correntista.setCnpj( "12345" );
		correntista.setRazaoSocial( null );
		correntista.setSaldo( 100.00 );
		
		List<Clientes> clientes = new ArrayList<Clientes>();
		clientes.add(cliente);
		correntista.setClientes(clientes);
		
		List<Agencias> agencias = new ArrayList<Agencias>();
		agencias.add( agencia );
		correntista.setAgencias( agencias );
		
		correntistasService.salvarCorrentistas( correntista );
		//
		correntista2.setTipo( TipoCorrentista.INVESTIMENTO );
		correntista2.setCnpj( "54321" );
		correntista2.setRazaoSocial( null );
		correntista2.setSaldo( 100.00 );
		
		List<Agencias> agencias2 = new ArrayList<Agencias>();
		agencias2.add( agencia2 );
		correntista2.setAgencias( agencias2 );
		
		correntistasService.salvarCorrentistas( correntista2 );
		//
		System.exit( 0 );
	}

}
