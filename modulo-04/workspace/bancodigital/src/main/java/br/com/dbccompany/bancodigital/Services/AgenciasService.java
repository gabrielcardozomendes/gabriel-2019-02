package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.AgenciasDAO;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasService {
	
	public static final AgenciasDAO AGENCIAS_DAO = new AgenciasDAO();
	public static final Logger LOG = Logger.getLogger( AgenciasService.class.getName() );
	
	public void salvarAgencias ( AgenciasDTO agenciasDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Agencias agencias = AGENCIAS_DAO.parseFrom( agenciasDTO );
		agencias.setNome( agenciasDTO.getNome() );
		
		try {
			Agencias agencia = AGENCIAS_DAO.buscar( 1 );
			if ( agencia == null ) {
				transaction.commit();
			} else {
				agencia.setId( agencia.getId() );
				AGENCIAS_DAO.atualizar( agencia );
			}
			
			AGENCIAS_DAO.criar( agencias );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarAgencias ( Agencias agencias ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
            Agencias agenciasRes = AGENCIAS_DAO.buscar(1);
            if(agenciasRes == null) {
                AGENCIAS_DAO.criar(agencias);
            }else {
                agencias.setId(agenciasRes.getId());
                AGENCIAS_DAO.atualizar(agencias);
            }
            
            if(started) {
                transaction.commit();
            }
            
        }catch (Exception e) {
            transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
		
		agencias.getId();
	}
}
