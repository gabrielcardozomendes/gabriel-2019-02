package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.BairrosDAO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosService {
	
	public static final BairrosDAO BAIRROS_DAO = new BairrosDAO();
	public static final Logger LOG = Logger.getLogger( BairrosService.class.getName() );
	
	public void salvarBairros ( BairrosDTO bairrosDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bairros bairros = BAIRROS_DAO.parseFrom( bairrosDTO );
		bairros.setNome( bairrosDTO.getNome() );
		
		try {
			Bairros bairro = BAIRROS_DAO.buscar( 1 );
			if ( bairro == null ) {
				transaction.commit();
			} else {
				bairro.setId( bairro.getId() );
				BAIRROS_DAO.atualizar( bairro );
			}
			
			BAIRROS_DAO.criar( bairro );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarBairros( Bairros bairros ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Bairros bairroRes = BAIRROS_DAO.buscar( 1 );
			if ( bairroRes == null ) {
				BAIRROS_DAO.criar( bairros );
			} else {
				bairros.setId( bairroRes.getId() );
				BAIRROS_DAO.atualizar( bairros );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
