package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.BancosDAO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Bancos;

public class BancosService {
	
	public static final BancosDAO BANCOS_DAO = new BancosDAO();
	public static final Logger LOG = Logger.getLogger( BancosService.class.getName() );
	
	public void salvarBancos ( BancosDTO bancosDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bancos bancos = BANCOS_DAO.parseFrom( bancosDTO );
		bancos.setNome( bancosDTO.getNome() );
		bancos.setCodigo( bancosDTO.getCodigo() );
		
		try {
			Bancos banco = BANCOS_DAO.buscar( bancosDTO.getIdBanco() );
			if ( banco == null ) {
				transaction.commit();
			} else {
				banco.setId( banco.getId() );
				BANCOS_DAO.atualizar( banco );
			}
			
			BANCOS_DAO.criar( bancos );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarBancos ( Bancos bancos ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Bancos banco = BANCOS_DAO.buscar( 1 );
			if ( banco == null ) {
				BANCOS_DAO.criar( bancos );
			} else {
				bancos.setId( banco.getId() );
				BANCOS_DAO.atualizar( bancos );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
