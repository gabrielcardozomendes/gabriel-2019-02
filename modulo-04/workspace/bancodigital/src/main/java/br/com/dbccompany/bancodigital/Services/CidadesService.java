package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.CidadesDAO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class CidadesService {
	
	public static final CidadesDAO CIDADES_DAO = new CidadesDAO();
	public static final Logger LOG = Logger.getLogger( CidadesService.class.getName() );
	
	public void salvarCidades ( CidadesDTO cidadeDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Cidades cidade = CIDADES_DAO.parseFrom( cidadeDTO );
		
		try {
			Cidades cidades = CIDADES_DAO.buscar( 1 );
			if ( cidades == null ) {
				transaction.commit();
			} else {
				cidades.setId( cidades.getId() );
				CIDADES_DAO.atualizar( cidades );
			}
			
			CIDADES_DAO.criar( cidade );
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarCidades ( Cidades cidadesO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Cidades cidades = CIDADES_DAO.buscar( 1 );
			if ( cidades == null ) {
				CIDADES_DAO.criar( cidadesO );
			} else {
				cidadesO.setId( cidades.getId() );
				CIDADES_DAO.atualizar( cidadesO );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
