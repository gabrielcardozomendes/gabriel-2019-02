package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.ClientesDAO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesService {
	
	public static final ClientesDAO CLIENTES_DAO = new ClientesDAO();
	public static final Logger LOG = Logger.getLogger( ClientesService.class.getName() );
	
	public void salvarClientes ( ClientesDTO clienteDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Clientes cliente = CLIENTES_DAO.parseFrom( clienteDTO );
		
		try {
			Clientes clientes = CLIENTES_DAO.buscar( 1 );
			if ( clientes == null ) {
				transaction.commit();
			} else {
				clientes.setId( clientes.getId() );
				CLIENTES_DAO.atualizar( clientes );
			}
			
			CLIENTES_DAO.criar( cliente );
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarClientes(Clientes clientes) {
        boolean started = HibernateUtil.beginTransction();
        Transaction transaction = HibernateUtil.getSession().getTransaction();
        try {
            Clientes clientesRes = CLIENTES_DAO.buscar(1);
            if (clientesRes == null) {
                CLIENTES_DAO.criar( clientes );
            } else {
                clientes.setId(clientesRes.getId());
                CLIENTES_DAO.atualizar( clientes );
            }
            
            if (started) {
                transaction.commit();
            }
            
        } catch (Exception e) {
            transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
