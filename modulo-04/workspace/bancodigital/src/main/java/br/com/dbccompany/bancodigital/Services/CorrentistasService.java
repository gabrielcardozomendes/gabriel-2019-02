package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.CorrentistasDAO;
import br.com.dbccompany.bancodigital.Dto.CorrentistaDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasService {
	
	public static final CorrentistasDAO CORRENTISTAS_DAO = new CorrentistasDAO();
	public static final Logger LOG = Logger.getLogger( CorrentistasService.class.getName() );
	
	public void salvarCorrentistas ( CorrentistaDTO correntistaDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Correntistas correntista = CORRENTISTAS_DAO.parseFrom( correntistaDTO );
		
		try {
			Correntistas correntistas = CORRENTISTAS_DAO.buscar( 1 );
			if ( correntistas == null ) {
				transaction.commit();
			} else {
				correntistas.setId( correntistas.getId() );
				CORRENTISTAS_DAO.atualizar( correntistas );
			}
			
			CORRENTISTAS_DAO.criar( correntista );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarCorrentistas ( Correntistas correntista ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Correntistas correntistas = CORRENTISTAS_DAO.buscar( 1 );
			if ( correntistas == null ) {
				CORRENTISTAS_DAO.criar( correntista );
			} else {
				correntista.setId( correntistas.getId() );
				CORRENTISTAS_DAO.atualizar( correntista );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
