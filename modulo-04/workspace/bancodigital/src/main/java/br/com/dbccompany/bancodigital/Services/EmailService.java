package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.EmailDAO;
import br.com.dbccompany.bancodigital.Dto.EmailDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Email;

public class EmailService {
	
	public static final EmailDAO EMAIL_DAO = new EmailDAO();
	public static final Logger LOG = Logger.getLogger( EmailService.class.getName() );
	
	public void salvarEmails ( EmailDTO emailDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Email email = EMAIL_DAO.parseFrom( emailDTO );
		
		try {
			Email emails = EMAIL_DAO.buscar( 1 );
			if ( emails == null ) {
				transaction.commit();
			} else {
				emails.setId( emails.getId() );
				EMAIL_DAO.atualizar( emails );
			}
			
			EMAIL_DAO.criar( email );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarEmails ( Email email) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Email emails = EMAIL_DAO.buscar( 1 );
			if ( emails == null ) {
				EMAIL_DAO.criar( email );
			} else {
				email.setId( emails.getId() );
				EMAIL_DAO.atualizar( email );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
