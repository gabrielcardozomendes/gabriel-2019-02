package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.EnderecosDAO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosService {
	
	public static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();
	public static final Logger LOG = Logger.getLogger( EnderecosService.class.getName() );
	
	public void salvarEnderecos ( EnderecosDTO enderecoDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Enderecos endereco = ENDERECOS_DAO.parseFrom( enderecoDTO );
		
		try {
			Enderecos enderecos = ENDERECOS_DAO.buscar( 1 );
			if ( enderecos == null ) {
				transaction.commit();
			} else {
				enderecos.setId( enderecos.getId() );
				ENDERECOS_DAO.atualizar( enderecos );
			}
			
			ENDERECOS_DAO.criar( endereco );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarEnderecos ( Enderecos endereco ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Enderecos enderecos = ENDERECOS_DAO.buscar( 1 );
			if ( enderecos == null ) {
				ENDERECOS_DAO.criar( endereco );
			} else {
				endereco.setId( enderecos.getId() );
				ENDERECOS_DAO.atualizar( endereco );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
