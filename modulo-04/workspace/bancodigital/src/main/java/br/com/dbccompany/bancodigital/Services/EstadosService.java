package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.EstadosDAO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosService {
	
	public static final EstadosDAO ESTADOS_DAO = new EstadosDAO();
	public static final Logger LOG = Logger.getLogger( EstadosService.class.getName() );
	
	public void salvarEstados ( EstadosDTO estadoDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Estados estado = ESTADOS_DAO.parseFrom( estadoDTO );
		
		try {
			Estados estados = ESTADOS_DAO.buscar( 1 );
			if ( estados == null ) {
				transaction.commit();
			} else {
				estados.setId( estados.getId() );
				ESTADOS_DAO.atualizar( estados );
			}
			
			ESTADOS_DAO.criar( estado );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarEstados(Estados estados) {
        boolean started = HibernateUtil.beginTransction();
        Transaction transaction = HibernateUtil.getSession().getTransaction();
        try {
            Estados estadosRes = ESTADOS_DAO.buscar(1);
            if (estadosRes == null) {
                ESTADOS_DAO.criar(estados);
            } else {
                estados.setId(estadosRes.getId());
                ESTADOS_DAO.atualizar(estados);
            }
            if (started) {
                transaction.commit();
            }
        } catch (Exception e) {
            transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
