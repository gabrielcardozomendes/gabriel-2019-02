package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.PaisesDAO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class PaisesService {
	
	public static final PaisesDAO PAISES_DAO = new PaisesDAO();
	public static final Logger LOG = Logger.getLogger( PaisesService.class.getName() );
	
	public void salvarPaises ( PaisesDTO paisDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Paises paises = PAISES_DAO.parseFrom( paisDTO );
		
		try {
			Paises paisesRes = PAISES_DAO.buscar( paisDTO.getIdPaises() );
			if ( paisesRes == null ) {
				PAISES_DAO.criar( paises );
			} else {
				paisesRes.setId( paisesRes.getId() );
				PAISES_DAO.atualizar( paisesRes );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarPaises( Paises paises ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Paises paisesRes = PAISES_DAO.buscar( 1 );
			if ( paisesRes == null ) {
				PAISES_DAO.criar( paises );
			} else {
				paises.setId( paisesRes.getId() );
				PAISES_DAO.atualizar( paises );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
