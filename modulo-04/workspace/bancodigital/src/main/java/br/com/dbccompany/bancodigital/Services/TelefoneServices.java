package br.com.dbccompany.bancodigital.Services;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.DAO.TelefonesDAO;
import br.com.dbccompany.bancodigital.Dto.TelefoneDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefoneServices {
	
	public static final TelefonesDAO TELEFONES_DAO = new TelefonesDAO();
	public static final Logger LOG = Logger.getLogger( TelefoneServices.class.getName() );
	
	public void salvarTelefones ( TelefoneDTO telefoneDTO ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Telefones telefone = TELEFONES_DAO.parseFrom( telefoneDTO );
		
		try {
			Telefones telefones = TELEFONES_DAO.buscar( 1 );
			if ( telefones == null ) {
				transaction.commit();
			} else {
				telefones.setId( telefones.getId() );
				TELEFONES_DAO.atualizar( telefones );
			}
			
			TELEFONES_DAO.criar( telefone );
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
	
	public void salvarTelefones ( Telefones telefone ) {
		boolean started = HibernateUtil.beginTransction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Telefones telefones = TELEFONES_DAO.buscar( 1 );
			if ( telefones == null ) {
				TELEFONES_DAO.criar( telefone );
			} else {
				telefone.setId( telefones.getId() );
				TELEFONES_DAO.atualizar( telefone );
			}
			
			if ( started ) {
				transaction.commit();
			}
		} catch ( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE,e.getMessage(), e );
		}
	}
}
